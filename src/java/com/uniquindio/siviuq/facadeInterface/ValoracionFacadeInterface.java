/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.facadeInterface;

import com.uniquindio.siviuq.entities.Valoracion;
import javax.ejb.Remote;

/**
 *
 * @author leon
 */
@Remote
public interface ValoracionFacadeInterface extends FacadeInterface<Valoracion>{
    
}
