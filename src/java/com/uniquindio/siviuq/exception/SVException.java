/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.exception;

/**
 *
 * @author Juliancho
 */
public class SVException extends Exception {
    private static final long serialVersionUID = -6545725061351418366L;
     private final String msgError;
    private final Exception exception;

    public SVException(String error, Exception ex) {
        super(error, ex);
        msgError = error;
        exception = ex;
    }
    
    public String getMsgException(){
        return msgError;
    }
    
    public Exception getException(){
        return exception;
    }
    
    
}
