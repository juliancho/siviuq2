/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.beans;


import com.uniquindio.siviuq.jsf.util.Utilidades;
import java.io.Serializable;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



/**
 *
 * @author Juliancho
 */
@ManagedBean(name="beanIdioma")
@SessionScoped
public class BeanIdioma implements Serializable{
    private String locale="es";
    private String idioma="Inglés";
    /**
     * Aca se realiza el cambio del Idioma
     * @return 
     */
    
    public String changeIdioma(){
        String nuevoLocale=getLocale().equals("es")?"en":"es";
        String nuevoIdioma=getLocale().equals("es")?"Español":"Inglés";
        Locale languaje=new Locale(nuevoLocale);
        setLocale(nuevoLocale);
        setIdioma(nuevoIdioma);
        Utilidades.getFC().getViewRoot().setLocale(languaje);
        return "";
    }
    
    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
    
    
}
