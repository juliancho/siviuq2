/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.beans;

import com.uniquindio.siviuq.entities.Activimetas;
import com.uniquindio.siviuq.facadeInterface.ActivimetasFacadeInterface;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juliancho
 */
@Stateless
public class ActivimetasFacade extends AbstractFacade<Activimetas> implements ActivimetasFacadeInterface{
    @PersistenceContext(unitName = "siviuq2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ActivimetasFacade() {
        super(Activimetas.class);
    }
    
}
