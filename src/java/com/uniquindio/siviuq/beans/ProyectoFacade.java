/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.beans;

import com.uniquindio.siviuq.entities.Proyecto;
import com.uniquindio.siviuq.facadeInterface.ProyectoFacadeInterface;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juliancho
 */
@Stateless
public class ProyectoFacade extends AbstractFacade<Proyecto>{
    @PersistenceContext(unitName = "siviuq2PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProyectoFacade() {
        super(Proyecto.class);
    }
    
}
