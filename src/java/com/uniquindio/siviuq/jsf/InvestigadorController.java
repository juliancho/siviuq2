package com.uniquindio.siviuq.jsf;

import com.uniquindio.siviuq.entities.Investigador;
import com.uniquindio.siviuq.jsf.util.JsfUtil;
import com.uniquindio.siviuq.jsf.util.PaginationHelper;
import com.uniquindio.siviuq.beans.InvestigadorFacade;
import com.uniquindio.siviuq.entities.Titulo;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "investigadorController")
@SessionScoped
public class InvestigadorController implements Serializable {

    private Investigador current;
    private DataModel items = null;
    @EJB
    private com.uniquindio.siviuq.beans.InvestigadorFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private UploadedFile file;
    private String mensaje;
    private List<Investigador> listaInvestigadores;
    private Investigador seleccionada;
    private List<Investigador> nuevalistaInvestigadores = new ArrayList();
   
    @ManagedProperty(value="#{tituloController}")
    private TituloController tituloController;
    

    
    
    
   public String cargarInvestigadores(){
       return null;
   }
    
    public String index(){
        return "verInformacionInvestigador.sv";
    }
    
    public List<Titulo> listaTitulos(){
        List<Titulo> li= (List)getSelected().getTituloCollection();
        System.out.println(li.get(0).getNombre());
        String titulos="";
        String coma="  ";
        for(int i=0; i<li.size();i++){
            titulos=coma+titulos;
            coma=" , ";
        }
        
        return null;
    }
    
    
    public InvestigadorController() {
    }

    public Investigador getSelected() {
        if (current == null) {
            current = new Investigador();
            selectedItemIndex = -1;
        }
        return current;
    }

    private InvestigadorFacade getFacade() {
        return ejbFacade;
    }
    
    public void Buscar(){
     
     current=getFacade().find(current.getIdinvestigador()); 
     System.out.println(current.getNombre());
    }
    
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

   
    
    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Investigador) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Investigador();
        selectedItemIndex = -1;
        return "crearInvestigador.sv";
    }

    public String create() {
       
        try {
            getFacade().create(current);
            setMensaje("Se creo el Investigador");
            
            return prepareCreate();
        } catch (Exception e) {
             setMensaje("Se presento un problema al crear el investigador");
        }
        return null;
    }

    public String prepareEdit() {
        current = (Investigador) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
       System.out.println(current.getIdinvestigador());
        try {
            getFacade().edit(current);
            setMensaje("El investigador se actualizo correctamente");
            current=null;
            return "crearInvestigador.sv";
        } catch (Exception e) {
            setMensaje("Se genero un problema al actualizar el investigador");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    
    public String remove() {
        Investigador u=getFacade().find(current.getIdinvestigador());
        
        try {
            getFacade().remove(u);
            setMensaje("Investigador Eliminado Exitosamente");
            current=null;
            return "crearInvestigador.sv";
        } catch (Exception e) {
            setMensaje("Error al eliminar el Investigador");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    public String destroy() {
        current = (Investigador) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("InvestigadorDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany2(List<Investigador> li) {
        return JsfUtil.getSelectItemsInvestigador(li, false);   
     }
    
    
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItemsInvestigador(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItemsInvestigador(ejbFacade.findAll(), true);
    }
    
    public SelectItem[] getItemsAvailableSelectOneTitulo() {
        
        return JsfUtil.getSelectItemsTitulo((List)getSelected().getTituloCollection(), true);
    }

    @FacesConverter(forClass = Investigador.class)
    public static class InvestigadorControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            InvestigadorController controller = (InvestigadorController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "investigadorController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Investigador) {
                Investigador o = (Investigador) object;
                return getStringKey(o.getIdinvestigador());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Investigador.class.getName());
            }
        }

    }

    public Investigador getCurrent() {
        return current;
    }

    public void setCurrent(Investigador current) {
        this.current = current;
    }

    public InvestigadorFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(InvestigadorFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }
    
    public void upload() {
        if(file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " creado correctamente.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            
            try {
                subirFichero("C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/curriculos/" + file.getFileName());
               
            } catch (IOException ex) {
                Logger.getLogger(InvestigadorController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }

    //permite enviar el file 
    private void subirFichero(String ruta) throws IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
       
        try {
         inputStream = file.getInputstream(); //leemos el fichero local
         // write the inputStream to a FileOutputStream
         outputStream = new FileOutputStream(new File(ruta));

         int read = 0;
         byte[] bytes = new byte[1024];

         while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
         }
          getSelected().setCurriculumruta("curriculos/" + file.getFileName());
        } finally {
             if (inputStream != null) {
            inputStream.close();
         }
         if (outputStream != null) {
            outputStream.close();
         }
        }
}
    
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<Investigador> getListaInvestigadores() {
        listaInvestigadores = ejbFacade.findAll();
        return listaInvestigadores;
    }

    public void setListaInvestigadores(List<Investigador> listaInvestigadores) {
        this.listaInvestigadores = listaInvestigadores;
    }

    public Investigador getSeleccionada() {
        return seleccionada;
    }

    public void setSeleccionada(Investigador seleccionada) {
        this.seleccionada = seleccionada;
    }

    public TituloController getTituloController() {
        return tituloController;
    }

    public void setTituloController(TituloController tituloController) {
        this.tituloController = tituloController;
    }

    public List<Investigador> getNuevalistaInvestigadores() {
        return nuevalistaInvestigadores;
    }

    public void setNuevalistaInvestigadores(List<Investigador> nuevalistaInvestigadores) {
        this.nuevalistaInvestigadores = nuevalistaInvestigadores;
    }

    
    

    
   
    
    
}
