package com.uniquindio.siviuq.jsf;

import com.uniquindio.siviuq.entities.Titulo;
import com.uniquindio.siviuq.jsf.util.JsfUtil;
import com.uniquindio.siviuq.jsf.util.PaginationHelper;
import com.uniquindio.siviuq.beans.TituloFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "tituloController")
@SessionScoped
public class TituloController implements Serializable {

    private Titulo current;
    private DataModel items = null;
    @EJB
    private com.uniquindio.siviuq.beans.TituloFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Titulo miTitulo;
    private List<Titulo> listaPorInv;
    private String tipoTitulo;
    private String mensaje;
    
    private Boolean valor=true;

    public void cambiarValor(){
      if(getTipoTitulo().equals("postgrado")){
          valor=false;
      }else{
          valor=true;
      }  
    }
    
    public TituloController() {
    }

    public Titulo getSelected() {
        if (current == null) {
            current = new Titulo();
            selectedItemIndex = -1;
        }
        return current;
    }

    private TituloFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Titulo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Titulo();
        selectedItemIndex = -1;
        return "";
    }

    public String create() {
        if(getTipoTitulo().equals("pregrado")){
            getSelected().setPregrado('1');
            getSelected().setPostgrado('0');
        } else{
            getSelected().setPregrado('0');
            getSelected().setPostgrado('1');
        }
       
        try {
            getFacade().create(current);
            setMensaje("Se creo el Titulo");
            
            return prepareCreate();
        } catch (Exception e) {
             setMensaje("Se presento un problema al crear el Titulo");
        }
        return null;
    }

    public String prepareEdit() {
        current = (Titulo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TituloUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Titulo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("TituloDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Titulo.class)
    public static class TituloControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TituloController controller = (TituloController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "tituloController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Titulo) {
                Titulo o = (Titulo) object;
                return getStringKey(o.getIdtitulo());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Titulo.class.getName());
            }
        }

    }

    public Titulo getCurrent() {
        return current;
    }

    public void setCurrent(Titulo current) {
        this.current = current;
    }

    public TituloFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(TituloFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public Titulo getMiTitulo() {
        return miTitulo;
    }

    public void setMiTitulo(Titulo miTitulo) {
        this.miTitulo = miTitulo;
    }

    public List<Titulo> getListaPorInv(String idInv) {
       listaPorInv = ejbFacade.findAll();
       System.out.println(listaPorInv.get(0).getNombre());
       List<Titulo> titulosInvestigador= new ArrayList();
       for(int i=0; i<listaPorInv.size(); i++) {
            if(listaPorInv.get(i).getIdinvestigador().getIdinvestigador().equals(idInv)){
                titulosInvestigador.add(listaPorInv.get(i));
            }
        }
        //System.out.println(titulosInvestigador.get(0).getNombre()+"listo");
        return titulosInvestigador;
    }

    public void setListaPorInv(List<Titulo> listaPorInv) {
        this.listaPorInv = listaPorInv;
    }

    public String getTipoTitulo() {
        return tipoTitulo;
    }

    public void setTipoTitulo(String tipoTitulo) {
        this.tipoTitulo = tipoTitulo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
   
    public Boolean getValor() {
        return valor;
    }

    public void setValor(Boolean valor) {
        this.valor = valor;
    }

    

    
    
}
