package com.uniquindio.siviuq.jsf;

import com.uniquindio.siviuq.entities.Proyecto;
import com.uniquindio.siviuq.jsf.util.JsfUtil;
import com.uniquindio.siviuq.jsf.util.PaginationHelper;
import com.uniquindio.siviuq.beans.ProyectoFacade;
import com.uniquindio.siviuq.entities.Convocatoria;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.swing.JFileChooser;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;

@ManagedBean(name = "proyectoController")
@SessionScoped
public class ProyectoController implements Serializable {

    private Proyecto current;
    private DataModel items = null;
    @EJB
    private com.uniquindio.siviuq.beans.ProyectoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Convocatoria convocatoria;
    private String mensaje;
    private String resumen;
    private String marcoteorico;
    private String impactosocial;
    private String planteamientoproblema;
    private String justificacion;
    private String metodologia;
    private String resultados;
    private String productos;
    private String bibliografia;
    private String trayectoria;
    private String aspectosbio;
    private String resumenresultado;
    private String informecientifico;
    private String resultadodirecto;
    private String resultadoindirecto;
    private String limitantes;
    private boolean paneles=true;
    private boolean panel2=false;
    private boolean botonGurdar=false;
    private boolean editar=true;
    
    
    
    public void cambiarBoolean(){
        if(paneles){
        paneles=false;
        panel2=true;
        }else{
         paneles=true;
        panel2=false;    
        }
    }
    
    
    
    public ProyectoController() {
    }

    public String guardarResumen(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"resumen.txt";
        
        guardarTexto(resumen,ruta);
        getSelected().setResumenruta(ruta);
        update();
        return null;
    }
    
    public String guardarMarcoTeorico(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"marcoteorico.txt";
        
        guardarTexto(marcoteorico,ruta);
        getSelected().setMarcoteoricoruta(ruta);
        update();
        return null;
    }
    
    public String guardarImpactoSocial(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"impactosocial.txt";
        
        guardarTexto(impactosocial,ruta);
        getSelected().setImpactosocialruta(ruta);
        update();
        return null;
    }
    
    public String guardarPlanteamientoProblema(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"planteamientoproblema.txt";
        
        guardarTexto(planteamientoproblema,ruta);
        getSelected().setPlanteamientoproblemaruta(ruta);
        update();
        return null;
    }
    
    public String guardarJustificacion(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"justificacion.txt";
        
        guardarTexto(justificacion,ruta);
        getSelected().setJustificacionruta(ruta);
        update();
        return null;
    }
    
    public String guardarMetodologia(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"metodologia.txt";
        
        guardarTexto(metodologia,ruta);
        getSelected().setMetodologiaruta(ruta);
        update();
        return null;
    }
    
    public String guardarResultados(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"resultados.txt";
        
        guardarTexto(resultados,ruta);
        getSelected().setResultadosesperadosruta(ruta);
        update();
        return null;
    }
    
    public String guardarProductos(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"productosesp.txt";
        
        guardarTexto(productos,ruta);
        getSelected().setProductosesperadosruta(ruta);
        update();
        return null;
    }
    
    public String guardarBibliografia(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"bibliografia.txt";
        
        guardarTexto(bibliografia,ruta);
        getSelected().setBibliografiaruta(ruta);
        update();
        return null;
    }
    
    public String guardarTrayectoria(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"trayectoria.txt";
        
        guardarTexto(trayectoria,ruta);
        getSelected().setTrayectoriagrupinvesruta(ruta);
        update();
        return null;
    }
    
    public String guardarBioetica(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"bioetica.txt";
        
        guardarTexto(aspectosbio,ruta);
        getSelected().setAspectobioeticoruta(ruta);
        update();
        return null;
    }
    
    public String guardarResumenResul(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"resumenresul.txt";
        
        guardarTexto(resumenresultado,ruta);
        getSelected().setResumenresultadosruta(ruta);
        update();
        return null;
    }
    
    public String guardarInformeCient(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"informecient.txt";
        
        guardarTexto(informecientifico,ruta);
        getSelected().setInformecientificoruta(ruta);
        update();
        return null;
    }
    
    
     public String guardarResultadoDirec(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"resultadodirec.txt";
        
        guardarTexto(resultadodirecto,ruta);
        getSelected().setResultadodirectoruta(ruta);
        update();
        return null;
    }
     
     public String guardarResultadoInDirec(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"resultadoindirec.txt";
        
        guardarTexto(resultadoindirecto,ruta);
        getSelected().setResultadoindirectoruta(ruta);
        update();
        return null;
    }
     
    public String guardarLimitantes(){
        //System.out.println("llegue");
        String ruta="C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/proyecto/"+current.getIdproyecto()+"limitantes.txt";
        
        guardarTexto(limitantes,ruta);
        getSelected().setLimitantesruta(ruta);
        update();
        return null;
    } 
    /*
    public void guardarArchivo(){
        //System.out.println(textoArea+"llego 1");
        guardarTexto(textoArea,"c:/curriculos/ensayo.txt");
        textoArea2=leerArchivo("c:/curriculos/ensayo.txt");
    }*/
    
    
    public void guardarTexto(String texto,String ruta){
         try{
            //System.out.println(texto+ruta+"llego 2");
            File guardar = new File(ruta); 
            //System.out.println(resumen+"llego 3");
            FileWriter archivo=new FileWriter(guardar);
            //System.out.println(resumen+"llego 4");
            archivo.write(texto);
            //System.out.println(resumen+"llego 5");
            archivo.close();
            setMensaje("se creo el archivo");

          }catch(IOException exp){
            System.out.println(exp);
          }
    }
    
    
    public String leerArchivo(String nombre){
    try{
      File f;
      FileReader lectorArchivo;
      f = new File(nombre);
      lectorArchivo = new FileReader(f);
      BufferedReader br = new BufferedReader(lectorArchivo);
      String l="";
      String aux="";/*variable auxiliar*/
      while(true){
          aux=br.readLine();
          if(aux!=null)
              l=l+aux+"\n";
          else
              break;
     }
     br.close();
     lectorArchivo.close();
     return l;
   }catch(IOException e){
   System.out.println("Error:"+e.getMessage());
   }
   return null;
}

    
    
    public void onTabChange(TabChangeEvent event) {
        FacesMessage msg = new FacesMessage("Vista Activada", "Activo: " + event.getTab().getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
         
    public void onTabClose(TabCloseEvent event) {
        FacesMessage msg = new FacesMessage("Vista Cerrada", "Cerro: " + event.getTab().getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    public Proyecto getSelected() {
        if (current == null) {
            current = new Proyecto();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ProyectoFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Proyecto) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Proyecto();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
       System.out.println("llegue a proyecto");
       System.out.println(current.getTitulo());
        try {
            getFacade().create(current);
            setMensaje("El proyecto fue creado con exito");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            setMensaje("Error al crear el proyecto");
            return null;
        }
    }

    public String prepareEdit() {
        current = (Proyecto) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            setMensaje("se actualizo correctamente el proyecto");
            
            return "crearProyecto.sv";
        } catch (Exception e) {
            setMensaje("Se genero un problema al actualizar el investigador");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Proyecto) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ProyectoDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItemsProyecto(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Proyecto.class)
    public static class ProyectoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProyectoController controller = (ProyectoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "proyectoController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Proyecto) {
                Proyecto o = (Proyecto) object;
                return getStringKey(o.getIdproyecto());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Proyecto.class.getName());
            }
        }

    }

    public Proyecto getCurrent() {
        return current;
    }

    public void setCurrent(Proyecto current) {
        this.current = current;
    }

    public ProyectoFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(ProyectoFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public Convocatoria getConvocatoria() {
        return convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria) {
        this.convocatoria = convocatoria;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isPaneles() {
        return paneles;
    }

    public void setPaneles(boolean paneles) {
        this.paneles = paneles;
    }

    public boolean isPanel2() {
        return panel2;
    }

    public void setPanel2(boolean panel2) {
        this.panel2 = panel2;
    }

    public boolean isBotonGurdar() {
        return botonGurdar;
    }

    public void setBotonGurdar(boolean botonGurdar) {
        this.botonGurdar = botonGurdar;
    }

    public boolean isEditar() {
        return editar;
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getMarcoteorico() {
        return marcoteorico;
    }

    public void setMarcoteorico(String marcoteorico) {
        this.marcoteorico = marcoteorico;
    }

    public String getImpactosocial() {
        return impactosocial;
    }

    public void setImpactosocial(String impactosocial) {
        this.impactosocial = impactosocial;
    }

    public String getPlanteamientoproblema() {
        return planteamientoproblema;
    }

    public void setPlanteamientoproblema(String planteamientoproblema) {
        this.planteamientoproblema = planteamientoproblema;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public String getMetodologia() {
        return metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }

    public String getResultados() {
        return resultados;
    }

    public void setResultados(String resultados) {
        this.resultados = resultados;
    }

    public String getProductos() {
        return productos;
    }

    public void setProductos(String productos) {
        this.productos = productos;
    }

    public String getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(String bibliografia) {
        this.bibliografia = bibliografia;
    }

    public String getTrayectoria() {
        return trayectoria;
    }

    public void setTrayectoria(String trayectoria) {
        this.trayectoria = trayectoria;
    }

    public String getAspectosbio() {
        return aspectosbio;
    }

    public void setAspectosbio(String aspectosbio) {
        this.aspectosbio = aspectosbio;
    }

    public String getResumenresultado() {
        return resumenresultado;
    }

    public void setResumenresultado(String resumenresultado) {
        this.resumenresultado = resumenresultado;
    }

    public String getInformecientifico() {
        return informecientifico;
    }

    public void setInformecientifico(String informecientifico) {
        this.informecientifico = informecientifico;
    }

    public String getResultadodirecto() {
        return resultadodirecto;
    }

    public void setResultadodirecto(String resultadodirecto) {
        this.resultadodirecto = resultadodirecto;
    }

    public String getResultadoindirecto() {
        return resultadoindirecto;
    }

    public void setResultadoindirecto(String resultadoindirecto) {
        this.resultadoindirecto = resultadoindirecto;
    }

    public String getLimitantes() {
        return limitantes;
    }

    public void setLimitantes(String limitantes) {
        this.limitantes = limitantes;
    }

    
    
    
}
