package com.uniquindio.siviuq.jsf;

import com.uniquindio.siviuq.entities.Institucion;
import com.uniquindio.siviuq.jsf.util.JsfUtil;
import com.uniquindio.siviuq.jsf.util.PaginationHelper;
import com.uniquindio.siviuq.beans.InstitucionFacade;
import com.uniquindio.siviuq.jsf.util.Utilidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;


@ManagedBean(name = "institucionController")
@SessionScoped
public class InstitucionController implements Serializable {

    private Institucion current;
    private DataModel items = null;
    @EJB
    private com.uniquindio.siviuq.beans.InstitucionFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<Institucion> listaInstituciones;
    private String mensaje="";
    
    public InstitucionController() {
    }

    public Institucion getSelected() {
        if (current == null) {
            current = new Institucion();
            selectedItemIndex = -1;
        }
        return current;
    }

    private InstitucionFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Institucion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Institucion();
        selectedItemIndex = -1;
        return "crearInstitucion.sv";
    }

    public String create() {
        try {
            getFacade().create(current);
            setMensaje("Se creo la institucion");
            
            return prepareCreate();
        } catch (Exception e) {
             setMensaje("Se presento un problema al crear la institucion");
        }
        return null;
    }

    public String prepareEdit() {
        current = (Institucion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        System.out.println(current.getIdinstitucion());
        try {
            getFacade().edit(current);
            setMensaje("La institucion se actualizo correctamente");
            current=null;
            return "crearInstitucion.sv";
        } catch (Exception e) {
            setMensaje("Se genero un problema al actualizar la institucion");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    public String remove() {
        Institucion u=getFacade().find(current.getIdinstitucion());
        
        try {
            getFacade().remove(u);
            setMensaje("La institucion se ha Eliminado Exitosamente");
            current=null;
            return "crearInstitucion.sv";
        } catch (Exception e) {
            setMensaje("Error al eliminar la institucion");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Institucion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("InstitucionDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItemsInstitucion(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Institucion.class)
    public static class InstitucionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            InstitucionController controller = (InstitucionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "institucionController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.math.BigDecimal getKey(String value) {
            java.math.BigDecimal key;
            key = new java.math.BigDecimal(value);
            return key;
        }

        String getStringKey(java.math.BigDecimal value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Institucion) {
                Institucion o = (Institucion) object;
                return getStringKey(o.getIdinstitucion());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Institucion.class.getName());
            }
        }

    }
    
    public Institucion getCurrent() {
        return current;
    }

    public void setCurrent(Institucion current) {
        this.current = current;
    }

    public InstitucionFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(InstitucionFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public List<Institucion> getListaInstituciones() {
       listaInstituciones= getFacade().findAll();
        return listaInstituciones;
    }

    public void setListaInstituciones(List<Institucion> listaInstituciones) {
        this.listaInstituciones = listaInstituciones;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    

}
