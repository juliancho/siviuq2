package com.uniquindio.siviuq.jsf;

import com.uniquindio.siviuq.entities.Evarexter;
import com.uniquindio.siviuq.jsf.util.JsfUtil;
import com.uniquindio.siviuq.jsf.util.PaginationHelper;
import com.uniquindio.siviuq.beans.EvarexterFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "evarexterController")
@SessionScoped
public class EvarexterController implements Serializable {

    private Evarexter current;
    private DataModel items = null;
    @EJB
    private com.uniquindio.siviuq.beans.EvarexterFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String mensaje="";
    private List<Evarexter> listaEvarexter;

    public EvarexterController() {
    }

    public Evarexter getSelected() {
        if (current == null) {
            current = new Evarexter();
            selectedItemIndex = -1;
        }
        System.out.println("paso c");
        return current;
    }

    private EvarexterFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Evarexter) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Evarexter();
        selectedItemIndex = -1;
        return "crearEvarexter.sv";
    }

    public String create() {
        try {
            getFacade().create(current);
            setMensaje("Se creo el evaluador externo");
            
            return prepareCreate();
        } catch (Exception e) {
             setMensaje("Se presento un problema al crear el evaluador externo");
        }
        return null;
    }

    public String prepareEdit() {
        current = (Evarexter) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        System.out.println(current.getIdevaluadorexterno());
        try {
            getFacade().edit(current);
            setMensaje("El evaluador externo se actualizo correctamente");
            current=null;
            return "crearEvarexter.sv";
        } catch (Exception e) {
            setMensaje("Se genero un problema al actualizar el evaluador externo");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Evarexter) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }
    
    
    public String remove() {
        Evarexter u=getFacade().find(current.getIdevaluadorexterno());
        
        try {
            getFacade().remove(u);
            setMensaje("El evaluador externo se ha Eliminado Exitosamente");
            current=null;
            return "crearEvarexter.sv";
        } catch (Exception e) {
            setMensaje("Error al eliminar el evaluador externo");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("EvarexterDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        System.out.println("paso d");
        return JsfUtil.getSelectItemsEvarexter(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Evarexter.class)
    public static class EvarexterControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EvarexterController controller = (EvarexterController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "evarexterController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Evarexter) {
                Evarexter o = (Evarexter) object;
                return getStringKey(o.getIdevaluadorexterno());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Evarexter.class.getName());
            }
        }

    }

    public Evarexter getCurrent() {
        return current;
    }

    public void setCurrent(Evarexter current) {
        this.current = current;
    }

    public EvarexterFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(EvarexterFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<Evarexter> getListaEvarexter() {
        listaEvarexter= getFacade().findAll();
        return listaEvarexter;
    }

    public void setListaEvarexter(List<Evarexter> listaEvarexter) {
        this.listaEvarexter = listaEvarexter;
    }
    
    

}
