/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.jsf.util;

import com.sun.jmx.snmp.SnmpString;
import com.uniquindio.siviuq.exception.SVException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Hex;




/**
 *
 * @author Juliancho
 */
public class Utilidades {
    
     public static SelectItem[] getSelectItems(List<?> entities, boolean selectOne) {
        int size = selectOne ? entities.size() + 1 : entities.size();
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        if (selectOne) {
            items[0] = new SelectItem("", "---");
            i++;
        }
        for (Object x : entities) {
            items[i++] = new SelectItem(x, x.toString());
        }
        return items;
    }

    public static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }
    
    /**
     * 
     * Funcion para obtener el objeto FAceContext del contexto de la aplicacion
     * 
     */
    
    public static FacesContext getFC(){
        return FacesContext.getCurrentInstance();
    }
    
    /**
     * 
     * Puncion para obtener el objeto ExternalContext del contexto de la aplicacion
     * 
     */
    
    public static ExternalContext getEC(){
        return getFC().getExternalContext();
    }
    
    /**
     * 
     * Funcion para obtener el objeto HttpServletRequest del contexto de la aplicacion
     * 
     */
    
    public static HttpServletRequest getRequest(){
        return (HttpServletRequest)getEC().getRequest();
    }
    
     /**
     * 
     * Funcion para obtener el objeto HttpServletResponse del contexto de la aplicacion
     * 
     */
    
    public static HttpServletResponse getResponse(){
        return (HttpServletResponse)getEC().getResponse();
    }
    
     /**
     * 
     * Funcion para obtener el objeto HttpSession del contexto de la aplicacion
     * 
     */
    
    public static HttpSession getSession(){
        return (HttpSession)getEC().getSession(false);
    }
    
     /**
     * 
     * Funcion para agregar mensajes al FAceContext
     * 
     */
    
    public static void addMsg(String titulo, String msg){
        FacesMessage mensaje=new FacesMessage(titulo, msg);
        FacesContext.getCurrentInstance().addMessage(null, mensaje);
    }
    
     /**
     * 
     * Funcion para agregar mensajes de tipo info al FaceContext
     * 
     */
    
    public static void addMsgInfo(String titulo, String msg){
       FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,titulo, msg));
    }

    /**
     * 
     * Funcion para agregar mensajes de warn al FaceContext
     * 
     */
    
    public static void addMsgWarn(String titulo, String msg){
       FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,titulo, msg));
    }
    
    /**
     * 
     * Funcion para agregar mensajes de tipo error al FaceContext
     * 
     */
    
    public static void addMsgError(String titulo, String msg){
       FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,titulo, msg));
    }
    
    /**
     * 
     * Metodo que permite encripar usando el metodo SHA512
     * 
     */
    
    public static String encriptPassword(String pass)throws SVException{
        try{
            MessageDigest md=MessageDigest.getInstance("SHA-512");
            md.update(pass.getBytes());
            byte[] mb=md.digest();
            return new String(Hex.encodeHex(mb));
        }catch(NoSuchAlgorithmException nsae){
            throw new SVException("Error al encriptar el password...", nsae);
        }
    }
    
}
