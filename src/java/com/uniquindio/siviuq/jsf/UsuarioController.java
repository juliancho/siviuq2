package com.uniquindio.siviuq.jsf;


import com.uniquindio.siviuq.entities.Usuario;
import com.uniquindio.siviuq.jsf.util.JsfUtil;
import com.uniquindio.siviuq.jsf.util.PaginationHelper;
import com.uniquindio.siviuq.beans.UsuarioFacade;
import com.uniquindio.siviuq.entities.Investigador;
import com.uniquindio.siviuq.entities.Proyecto;
import com.uniquindio.siviuq.entities.Titulo;
import com.uniquindio.siviuq.exception.SVException;
import com.uniquindio.siviuq.jsf.util.Utilidades;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import javax.validation.Valid;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "usuarioController")
@SessionScoped
public class UsuarioController implements Serializable {

    @Valid
    private Usuario current;
    private DataModel items = null;
    @EJB
    private UsuarioFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String mensaje="";
    private Boolean status;
    private List<Usuario> listaUsuarios;
    private String clavever;
    private String mensajeClave="";
    private Usuario usuarioValidado;
    private Boolean menuFacultad=true;
    private Boolean menuPrograma=true;
    private Boolean menuInvestigador=true;
    private UploadedFile file;
    private Titulo titulo;
    private String idUsuario="";
    private String mensajecargarM="";
    private boolean id=false;
    private int ind=0;

    
    @ManagedProperty(value="#{investigadorController}")
    private InvestigadorController investigadorController;
    
    @ManagedProperty(value="#{tituloController}")
    private TituloController tituloController;
    
    List<Investigador> li= new ArrayList();
    List<Investigador> newli=new ArrayList();
    List<Investigador> listaSeleccionada=new ArrayList();
    
    @ManagedProperty(value="#{proyectoController}")
    private ProyectoController proyectoController;
    
   
    
    
    
    public SelectItem[] darItemsInvestigadores(){
       
        if(ind==0){
           li=investigadorController.getListaInvestigadores();  
           ind++;
        }
        
        for(int i=0; i<li.size();i++){
            if(li.get(i).getIdinvestigador().equals(getSelected().getIdinvestigador().getIdinvestigador())){
                newli.add(li.get(i));
                li.remove(i);
            }
            for(int k=0; k<listaSeleccionada.size();k++){
                if(li.get(i).getIdinvestigador().equals(listaSeleccionada.get(k).getIdinvestigador())){
                   newli.add(li.get(i));
                    li.remove(i);
                }
            }
        }
        return investigadorController.getItemsAvailableSelectMany2(li);
    }
    
    public SelectItem[] darItemsInvestigadoresSelec(){
        
        return investigadorController.getItemsAvailableSelectMany2(newli);
    }
    
    public String cargarListaInvestigadores(){
        
      return "crearProyecto.sv";  
    }
    
    public void guardarId(){
        proyectoController.getSelected().setIdgrupoinvestigacion(current.getIdinvestigador().getIdgrupoinv());
       
       
    }
     public String darListener(){
        
       if(proyectoController.getCurrent()!=null){
                setId(true);
                proyectoController.setPaneles(false);
                proyectoController.setPanel2(true);
                proyectoController.setBotonGurdar(true);
                if(proyectoController.getSelected().getResumenruta()==null){
                    proyectoController.setResumen(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResumenruta());
                     proyectoController.setResumen(resumen);
                 }
                 if(proyectoController.getSelected().getMarcoteoricoruta()==null){
                    proyectoController.setMarcoteorico(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getMarcoteoricoruta());
                     proyectoController.setMarcoteorico(resumen);
                 }
                 if(proyectoController.getSelected().getImpactosocialruta()==null){
                    proyectoController.setImpactosocial(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getImpactosocialruta());
                     proyectoController.setImpactosocial(resumen);
                 }
                 if(proyectoController.getSelected().getPlanteamientoproblemaruta()==null){
                    proyectoController.setPlanteamientoproblema(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getPlanteamientoproblemaruta());
                     proyectoController.setPlanteamientoproblema(resumen);
                 }
                 if(proyectoController.getSelected().getJustificacionruta()==null){
                    proyectoController.setJustificacion(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getJustificacionruta());
                     proyectoController.setJustificacion(resumen);
                 }
                 if(proyectoController.getSelected().getMetodologiaruta()==null){
                    proyectoController.setMetodologia(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getMetodologiaruta());
                     proyectoController.setMetodologia(resumen);
                 }
                 if(proyectoController.getSelected().getResultadosesperadosruta()==null){
                    proyectoController.setResultados(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResultadosesperadosruta());
                     proyectoController.setResultados(resumen);
                 }
                 if(proyectoController.getSelected().getProductosesperadosruta()==null){
                    proyectoController.setProductos(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getProductosesperadosruta());
                     proyectoController.setProductos(resumen);
                 }
                 if(proyectoController.getSelected().getBibliografiaruta()==null){
                    proyectoController.setBibliografia(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getBibliografiaruta());
                     proyectoController.setBibliografia(resumen);
                 }
                 if(proyectoController.getSelected().getTrayectoriagrupinvesruta()==null){
                    proyectoController.setTrayectoria(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getTrayectoriagrupinvesruta());
                     proyectoController.setTrayectoria(resumen);
                 }
                 if(proyectoController.getSelected().getAspectobioeticoruta()==null){
                    proyectoController.setAspectosbio(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getAspectobioeticoruta());
                     proyectoController.setAspectosbio(resumen);
                 }
                 if(proyectoController.getSelected().getResumenresultadosruta()==null){
                    proyectoController.setResumenresultado(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResumenresultadosruta());
                     proyectoController.setResumenresultado(resumen);
                 }
                 if(proyectoController.getSelected().getInformecientificoruta()==null){
                     proyectoController.setInformecientifico(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getInformecientificoruta());
                     proyectoController.setInformecientifico(resumen);
                 }
                 if(proyectoController.getSelected().getResultadodirectoruta()==null){
                     proyectoController.setResultadodirecto(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResultadodirectoruta());
                     proyectoController.setResultadodirecto(resumen);
                 }
                 if(proyectoController.getSelected().getResultadoindirectoruta()==null){
                     proyectoController.setResultadoindirecto(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResultadoindirectoruta());
                     proyectoController.setResultadoindirecto(resumen);
                 }
                 if(proyectoController.getSelected().getLimitantesruta()==null){
                     proyectoController.setLimitantes(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getLimitantesruta());
                     proyectoController.setLimitantes(resumen);
                 }
                 
       }else{
                if(proyectoController.getSelected().getResumenruta().equals("")){
                       proyectoController.setResumen("..");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResumenruta());

                     proyectoController.setResumen(resumen);
                 }
                 if(proyectoController.getSelected().getMarcoteoricoruta()==null){
                    proyectoController.setMarcoteorico(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getMarcoteoricoruta());
                     proyectoController.setMarcoteorico(resumen);
                 }
                 if(proyectoController.getSelected().getImpactosocialruta()==null){
                    proyectoController.setImpactosocial(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getImpactosocialruta());
                     proyectoController.setImpactosocial(resumen);
                 }
                 if(proyectoController.getSelected().getPlanteamientoproblemaruta()==null){
                      proyectoController.setPlanteamientoproblema(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getPlanteamientoproblemaruta());
                     proyectoController.setPlanteamientoproblema(resumen);
                 }
                 if(proyectoController.getSelected().getJustificacionruta()==null){
                    proyectoController.setJustificacion(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getJustificacionruta());
                     proyectoController.setJustificacion(resumen);
                 }
                 if(proyectoController.getSelected().getMetodologiaruta()==null){
                    proyectoController.setMetodologia(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getMetodologiaruta());
                     proyectoController.setMetodologia(resumen);
                 }
                  if(proyectoController.getSelected().getResultadosesperadosruta()==null){
                    proyectoController.setResultados(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResultadosesperadosruta());
                     proyectoController.setResultados(resumen);
                 }
                 if(proyectoController.getSelected().getProductosesperadosruta()==null){
                    proyectoController.setProductos(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getProductosesperadosruta());
                     proyectoController.setProductos(resumen);
                 } 
                 if(proyectoController.getSelected().getBibliografiaruta()==null){
                    proyectoController.setBibliografia(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getBibliografiaruta());
                     proyectoController.setBibliografia(resumen);
                 }
                 if(proyectoController.getSelected().getTrayectoriagrupinvesruta()==null){
                    proyectoController.setTrayectoria(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getTrayectoriagrupinvesruta());
                     proyectoController.setTrayectoria(resumen);
                 }
                 if(proyectoController.getSelected().getAspectobioeticoruta()==null){
                    proyectoController.setAspectosbio(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getAspectobioeticoruta());
                     proyectoController.setAspectosbio(resumen);
                 }
                 if(proyectoController.getSelected().getResumenresultadosruta()==null){
                    proyectoController.setResumenresultado(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResumenresultadosruta());
                     proyectoController.setResumenresultado(resumen);
                 }
                 if(proyectoController.getSelected().getInformecientificoruta()==null){
                    proyectoController.setInformecientifico(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getInformecientificoruta());
                     proyectoController.setInformecientifico(resumen);
                 }
                 if(proyectoController.getSelected().getResultadodirectoruta()==null){
                     proyectoController.setResultadodirecto(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResultadodirectoruta());
                     proyectoController.setResultadodirecto(resumen);
                 }
                 if(proyectoController.getSelected().getResultadoindirectoruta()==null){
                     proyectoController.setResultadoindirecto(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getResultadoindirectoruta());
                     proyectoController.setResultadoindirecto(resumen);
                 }
                 if(proyectoController.getSelected().getLimitantesruta()==null){
                     proyectoController.setLimitantes(" ");
                 }else{
                     String resumen=proyectoController.leerArchivo(proyectoController.getSelected().getLimitantesruta());
                     proyectoController.setLimitantes(resumen);
                 }
                setId(false);
                proyectoController.setPaneles(true);
                proyectoController.setPanel2(false);
                proyectoController.setBotonGurdar(false);
       }
        
         return "crearProyecto.sv";
    }
    
    public String guardarProyecto(){
        proyectoController.create();
        setId(true);
        proyectoController.cambiarBoolean();
        proyectoController.setEditar(false);
        proyectoController.setBotonGurdar(true);
        return "crearProyecto.sv";
        
    }
    
    public String actualizarProyecto(){
        proyectoController.setPaneles(true);
        proyectoController.setPanel2(false);
        proyectoController.setBotonGurdar(true);
        proyectoController.setEditar(false);
        
        return "/crearProyecto.sv";
    }
    
    
    public String editarProyecto(){
        proyectoController.update();
        proyectoController.setPaneles(false);
        proyectoController.setPanel2(true);
        proyectoController.setBotonGurdar(true);
        proyectoController.setEditar(true);
        return "crearProyecto.sv";
    }
    
    
    public void borrarController(){
        current=null;
        setMensaje("");
    }
    
    public String refrescar(){
        return "/editarInvestigador.sv";
    }
    
    
    public String misTitulos(){
        tituloController.getSelected().setIdtitulo("13fguuujjuu888");
        tituloController.getSelected().setIdinvestigador(usuarioValidado.getIdinvestigador());
        
        List<Titulo> li=new ArrayList();
        li= tituloController.getListaPorInv(usuarioValidado.getIdinvestigador().getIdinvestigador());
        //System.out.println(li.get(0).getNombre()+"  llego a usuario");
        String titulo="";
        String coma= "";
        for(int i=0; i<li.size();i++){
           titulo= titulo+coma+li.get(i).getNombre();
           coma=" , ";
        }
        tituloController.getSelected().setIdtitulo("");
        return titulo;
    }
    
    public String crearTitulo(){
     tituloController.getSelected().setIdinvestigador(usuarioValidado.getIdinvestigador());
     String r=tituloController.create();
     return r;
    }
    
    public String editarInv(){
        investigadorController.setCurrent(getSelected().getIdinvestigador());
        System.out.println("paso");
        System.out.println(investigadorController.getCurrent().getIdinvestigador());
        investigadorController.update();
        return "editarInvestigador";
    }
    
    public String uploadInv(){
       String extension = "";
            System.out.println("llego");
            int i = file.getFileName().lastIndexOf('.');
            System.out.println(""+i);
            if (i > 0) {
                extension = file.getFileName().substring(i+1);
            }
            System.out.println(extension);
            if(extension.equals("pdf")){
                setMensajecargarM("");
                investigadorController.setCurrent(getSelected().getIdinvestigador());
                investigadorController.setFile(file);
                investigadorController.upload();
                //System.out.println("paso");
                //System.out.println(investigadorController.getCurrent().getIdinvestigador());
                investigadorController.update();
                setMensajecargarM("");
            }
            setMensajecargarM("La extensión del archivo debe ser .pdf");
        return null;
    }
    
    
    public String validarUsuario(){
        
        try {
            getSelected().setClave(Utilidades.encriptPassword(getSelected().getClave()));
            System.out.println("User : "+getSelected().getIdusuario());
            System.out.println("Password : "+getSelected().getClave());
                        
             Usuario encontrado=getFacade().find(current.getIdusuario());
             if(encontrado==null){
                 setMensaje("Usuario no encontrado");
             }else{
                 setMensaje("Usuario Encontrado");
                 ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                        if(encontrado.getClave().equals(current.getClave())){
                            usuarioValidado=encontrado;
                            current=encontrado;
                            Map<String, Object> sessionMap = externalContext.getSessionMap();
                                    
                            setStatus(true);
                            if(encontrado.getRol().equals("Invetigador"))
                                investigadorController.setCurrent(usuarioValidado.getIdinvestigador());
        
                           return encontrado.getUrl();
                        }else{
                            setMensaje("Validación incorrecta");  
                            ((HttpServletRequest)externalContext.getRequest()).setAttribute("errorAccesos", getMensaje());
                             return "login";
                        }
                }        
            } catch (SVException ex) {
             Utilidades.addMsgError("Error en validarLogin", ex.getMsgException());
            }
         return "login";
    }

    public String cambiarValor(){
        if(getSelected().getRol().equals("Investigador")){
         setMenuInvestigador(false);
         setMenuFacultad(true);
         setMenuPrograma(true);
        }else if(getSelected().getRol().equals("Programa")){
         setMenuPrograma(false); 
         setMenuFacultad(true);
         setMenuInvestigador(true);
        }else if (getSelected().getRol().equals("Facultad")){
            setMenuFacultad(false);
            setMenuInvestigador(true);
            setMenuPrograma(true);
        }else{
            setMenuFacultad(true);
            setMenuInvestigador(true);
            setMenuPrograma(true);
        }
        
        
        return "gestionUsuarios.sv";
    }
    
    
    public UsuarioController() {
    }

    public Usuario getSelected() {
        if (current == null) {
            current = new Usuario();
            selectedItemIndex = -1;
        }
        return current;
    }

    private UsuarioFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Usuario();
        selectedItemIndex = -1;
        return "gestionUsuarios.sv";
    }

    public String create() {
        current.setIdusuario(idUsuario);
        if(getSelected().getRol().equals("Investigador")){
            getSelected().setUrl("investigador.sv");
        }else if(getSelected().getRol().equals("Programa")){
            getSelected().setUrl("programa.sv");
        }else if (getSelected().getRol().equals("Facultad")){
                getSelected().setUrl("facultad.sv");
        }else if (getSelected().getRol().equals("BioEtica")){
                getSelected().setUrl("bioetica.sv");
        }else if (getSelected().getRol().equals("Vicerrectoria")){
                getSelected().setUrl("vicerrectoria.sv");
        }else{
            setMensaje("Debe Seleccionar un Rol");
            return null;
        }
        
        if(getSelected().getClave().equals(getClavever())){
         
        try {
            getSelected().setClave(Utilidades.encriptPassword(getSelected().getClave()));
            getFacade().create(current);
            setMensaje("Usuario creado correctamente");
            setMensajeClave("");
            Thread.sleep(1000);
            setMensaje("");
            setMenuFacultad(true);
            setMenuInvestigador(true);
            setMenuPrograma(true);
            setIdUsuario("");
            return prepareCreate();
        } catch (Exception e) {
            setMensaje("Error al crear el Usuario");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
        }else{
            setMensajeClave("Las claves no coinciden");
            return null;
        }
    }

    public String prepareEdit() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "gestionUsuarios.sv";
    }

    public String update() {
         current.setIdusuario(idUsuario);
        if(getSelected().getRol().equals("Investigador")){
                getSelected().setUrl("investigador.sv");
                getSelected().setIdfacultad(null);
                getSelected().setIdprograma(null);
        }else if(getSelected().getRol().equals("Programa")){
                getSelected().setUrl("programa.sv");
                getSelected().setIdfacultad(null);
                getSelected().setIdinvestigador(null);
                
        }else if (getSelected().getRol().equals("Facultad")){
                getSelected().setUrl("facultad.sv");
                getSelected().setIdinvestigador(null);
                getSelected().setIdprograma(null);
        }else if (getSelected().getRol().equals("BioEtica")){
                getSelected().setUrl("bioetica.sv");
                getSelected().setIdfacultad(null);
                getSelected().setIdinvestigador(null);
                getSelected().setIdprograma(null);
        }else if (getSelected().getRol().equals("Vicerrectoria")){
                getSelected().setUrl("vicerrectoria.sv");
                getSelected().setIdfacultad(null);
                getSelected().setIdinvestigador(null);
                getSelected().setIdprograma(null);
        }else{
            setMensaje("Debe Seleccionar un Rol");
            return null;
        }
        
        
        if(getSelected().getClave().equals(getClavever())){
            
            try {
                getSelected().setClave(Utilidades.encriptPassword(getSelected().getClave()));
                getFacade().edit(current);
                setMensaje("Usuario Actualizado");
                setMensajeClave("");
                Thread.sleep(1000);
                setMensaje("");
                setMenuFacultad(true);
                setMenuInvestigador(true);
                setMenuPrograma(true);
                setIdUsuario("");
                return "gestionUsuarios.sv";
            } catch (Exception e) {
                setMensaje("Error al actualizar Usuario");
                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
                return null;
            }
        }else{
            setMensajeClave("Las claves no coinciden");
            return null;
        }
    }

    
    public String remove() {
         current.setIdusuario(idUsuario);
        System.out.println(current.getIdusuario());
        Usuario u=getFacade().find(current.getIdusuario());
        System.out.println(u.getIdusuario());
        try {
            getFacade().remove(u);
            setMensaje("Usuario Eliminado Exitosamente");
            Thread.sleep(1000);
            setMenuFacultad(true);
            setMenuInvestigador(true);
            setMenuPrograma(true);
            
            setIdUsuario("");
            return "gestionUsuarios.sv";
        } catch (Exception e) {
        setMensaje("Error al eliminar Usuario");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    public String destroy() {
        current = (Usuario) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }
    
   private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("UsuarioDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Usuario.class)
    public static class UsuarioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsuarioController controller = (UsuarioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usuarioController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usuario) {
                Usuario o = (Usuario) object;
                return getStringKey(o.getIdusuario());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Usuario.class.getName());
            }
        }

    }

    public Usuario getCurrent() {
        return current;
    }

    public void setCurrent(Usuario current) {
        this.current = current;
    }

    public UsuarioFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(UsuarioFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Usuario> getListaUsuarios() {
        listaUsuarios= ejbFacade.findAll();
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String getClavever() {
        return clavever;
    }

    public void setClavever(String clavever) {
        this.clavever = clavever;
    }

    public String getMensajeClave() {
        return mensajeClave;
    }

    public void setMensajeClave(String mensajeClave) {
        this.mensajeClave = mensajeClave;
    }

    public Usuario getUsuarioValidado() {
        return usuarioValidado;
    }

    public void setUsuarioValidado(Usuario usuarioValidado) {
        this.usuarioValidado = usuarioValidado;
    }

    public Boolean getMenuFacultad() {
        return menuFacultad;
    }

    public void setMenuFacultad(Boolean menuFacultad) {
        this.menuFacultad = menuFacultad;
    }

    public Boolean getMenuPrograma() {
        return menuPrograma;
    }

    public void setMenuPrograma(Boolean menuPrograma) {
        this.menuPrograma = menuPrograma;
    }

    public Boolean getMenuInvestigador() {
        return menuInvestigador;
    }

    public void setMenuInvestigador(Boolean menuInvestigador) {
        this.menuInvestigador = menuInvestigador;
    }

    public InvestigadorController getInvestigadorController() {
        return investigadorController;
    }

    public void setInvestigadorController(InvestigadorController investigadorController) {
        this.investigadorController = investigadorController;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public TituloController getTituloController() {
        return tituloController;
    }

    public void setTituloController(TituloController tituloController) {
        this.tituloController = tituloController;
    }

    public Titulo getTitulo() {
        return titulo;
    }

    public void setTitulo(Titulo titulo) {
        this.titulo = titulo;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getMensajecargarM() {
        return mensajecargarM;
    }

    public void setMensajecargarM(String mensajecargarM) {
        this.mensajecargarM = mensajecargarM;
    }

    public ProyectoController getProyectoController() {
        return proyectoController;
    }

    public void setProyectoController(ProyectoController proyectoController) {
        this.proyectoController = proyectoController;
    }

    public boolean isId() {
        return id;
    }

    public void setId(boolean id) {
        this.id = id;
    }

    public List<Investigador> getLi() {
        return li;
    }

    public void setLi(List<Investigador> li) {
        this.li = li;
    }

    public List<Investigador> getNewli() {
        return newli;
    }

    public void setNewli(List<Investigador> newli) {
        this.newli = newli;
    }

    public List<Investigador> getListaSeleccionada() {
        return listaSeleccionada;
    }

    public void setListaSeleccionada(List<Investigador> listaSeleccionada) {
        this.listaSeleccionada = listaSeleccionada;
    }

    public int getInd() {
        return ind;
    }

    public void setInd(int ind) {
        this.ind = ind;
    }

    
    
    
}
