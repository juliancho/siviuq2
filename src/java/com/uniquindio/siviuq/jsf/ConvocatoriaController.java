package com.uniquindio.siviuq.jsf;

import com.uniquindio.siviuq.entities.Convocatoria;
import com.uniquindio.siviuq.jsf.util.JsfUtil;
import com.uniquindio.siviuq.jsf.util.PaginationHelper;
import com.uniquindio.siviuq.beans.ConvocatoriaFacade;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "convocatoriaController")
@SessionScoped
public class ConvocatoriaController implements Serializable {

    private Convocatoria current;
    private DataModel items = null;
    @EJB
    private com.uniquindio.siviuq.beans.ConvocatoriaFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<Convocatoria> listaConvocatorias;
    private UploadedFile file=null;
    private String mensaje;
    private String url="";
    private Convocatoria seleccionada;

    
    public String darListener(){
        return "mostrarConvocatoria.sv";
    }
    
    public ConvocatoriaController() {
    }

    public Convocatoria getSelected() {
        if (current == null) {
            current = new Convocatoria();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ConvocatoriaFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Convocatoria) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Convocatoria();
        selectedItemIndex = -1;
        return "vistaConvocatoria.sv";
    }

    public String create() {
       if(!getUrl().equals("")){
            try {
                System.out.println(url+"<-esta es la url");
                getFacade().create(current);
                setMensaje("Se creo la Convocatoria");
                setUrl("");
                return prepareCreate();
            } catch (Exception e) {
                 setMensaje("Se presento un problema al crear la convocatoria");
            }
       }else
           setMensaje("Se de subir un archivo pdf de la convocatoria");
        return "vistaConvocatoria.sv";
    }

    public String prepareEdit() {
        current = (Convocatoria) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
         try {
            getFacade().edit(current);
            setMensaje("La convocatoria se actualizó correctamente");
            
            return "vistaConvocatoria.sv";
        } catch (Exception e) {
            setMensaje("Se genero un problema al actualizar la convocatoria");
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Convocatoria) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("ConvocatoriaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItemsConvo(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Convocatoria.class)
    public static class ConvocatoriaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ConvocatoriaController controller = (ConvocatoriaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "convocatoriaController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Convocatoria) {
                Convocatoria o = (Convocatoria) object;
                return getStringKey(o.getIdconvocatoria());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Convocatoria.class.getName());
            }
        }

    }

    public Convocatoria getCurrent() {
        return current;
    }

    public void setCurrent(Convocatoria current) {
        this.current = current;
    }

    public ConvocatoriaFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(ConvocatoriaFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public List<Convocatoria> getListaConvocatorias() {
        listaConvocatorias= ejbFacade.findAll();
        return listaConvocatorias;
    }

    public void setListaConvocatorias(List<Convocatoria> listaConvocatorias) {
        this.listaConvocatorias = listaConvocatorias;
    }

    public void upload() {
            String extension = "";
            System.out.println("llego");
            int i = file.getFileName().lastIndexOf('.');
            System.out.println(""+i);
            if (i > 0) {
                extension = file.getFileName().substring(i+1);
            }
            System.out.println(extension);
            if(extension.equals("pdf")){
                FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " fue cargado con exito.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                //System.out.println("eso");

                try {
                    setUrl("convocatorias/" + file.getFileName());
                    subirFichero("C:/Users/Juliancho/Documents/NetBeansProjects/siviuq2/web/resources/convocatorias/" + file.getFileName());
                    //System.out.println("c:/convocatorias/" + file.getFileName());
                     
                } catch (IOException ex) {
                    Logger.getLogger(InvestigadorController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
               FacesMessage message = new FacesMessage("Error", file.getFileName() + " debe tener extension PDF.");
               FacesContext.getCurrentInstance().addMessage(null, message); 
               setUrl("");
            }
        
    }

    //permite enviar el file 
    private void subirFichero(String ruta) throws IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        //System.out.println(ruta);
        try {
         inputStream = file.getInputstream(); //leemos el fichero local
         // write the inputStream to a FileOutputStream
         outputStream = new FileOutputStream(new File(ruta));

         int read = 0;
         byte[] bytes = new byte[2000000];

         while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
         }
           // setMensaje(ruta);
         getSelected().setDescripcionruta(url);
         
        } finally {
             if (inputStream != null) {
            inputStream.close();
         }
         if (outputStream != null) {
            outputStream.close();
         }
        }
     }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Convocatoria getSeleccionada() {
        return seleccionada;
    }

    public void setSeleccionada(Convocatoria seleccionada) {
        this.seleccionada = seleccionada;
    }
    
    
    
}
