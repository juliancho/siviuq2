/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "COMPRAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compras.findAll", query = "SELECT c FROM Compras c"),
    @NamedQuery(name = "Compras.findByIdequipos", query = "SELECT c FROM Compras c WHERE c.idequipos = :idequipos"),
    @NamedQuery(name = "Compras.findByDescripcion", query = "SELECT c FROM Compras c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Compras.findByJustificacion", query = "SELECT c FROM Compras c WHERE c.justificacion = :justificacion"),
    @NamedQuery(name = "Compras.findByValorunitario", query = "SELECT c FROM Compras c WHERE c.valorunitario = :valorunitario"),
    @NamedQuery(name = "Compras.findByCantidad", query = "SELECT c FROM Compras c WHERE c.cantidad = :cantidad"),
    @NamedQuery(name = "Compras.findByValortotal", query = "SELECT c FROM Compras c WHERE c.valortotal = :valortotal"),
    @NamedQuery(name = "Compras.findByEquipos", query = "SELECT c FROM Compras c WHERE c.equipos = :equipos"),
    @NamedQuery(name = "Compras.findBySoftware", query = "SELECT c FROM Compras c WHERE c.software = :software"),
    @NamedQuery(name = "Compras.findByMateriales", query = "SELECT c FROM Compras c WHERE c.materiales = :materiales"),
    @NamedQuery(name = "Compras.findByBibliografia", query = "SELECT c FROM Compras c WHERE c.bibliografia = :bibliografia"),
    @NamedQuery(name = "Compras.findByRubro1", query = "SELECT c FROM Compras c WHERE c.rubro1 = :rubro1"),
    @NamedQuery(name = "Compras.findByRubro2", query = "SELECT c FROM Compras c WHERE c.rubro2 = :rubro2"),
    @NamedQuery(name = "Compras.findByRubro3", query = "SELECT c FROM Compras c WHERE c.rubro3 = :rubro3"),
    @NamedQuery(name = "Compras.findByRubro4", query = "SELECT c FROM Compras c WHERE c.rubro4 = :rubro4")})
public class Compras implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDEQUIPOS")
    private String idequipos;
    @Size(max = 500)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Size(max = 1000)
    @Column(name = "JUSTIFICACION")
    private String justificacion;
    @Column(name = "VALORUNITARIO")
    private BigInteger valorunitario;
    @Column(name = "CANTIDAD")
    private BigInteger cantidad;
    @Column(name = "VALORTOTAL")
    private BigInteger valortotal;
    @Column(name = "EQUIPOS")
    private Character equipos;
    @Column(name = "SOFTWARE")
    private Character software;
    @Column(name = "MATERIALES")
    private Character materiales;
    @Column(name = "BIBLIOGRAFIA")
    private Character bibliografia;
    @Column(name = "RUBRO1")
    private BigInteger rubro1;
    @Column(name = "RUBRO2")
    private BigInteger rubro2;
    @Column(name = "RUBRO3")
    private BigInteger rubro3;
    @Column(name = "RUBRO4")
    private BigInteger rubro4;
    @JoinColumn(name = "IDPRESUPUESTO", referencedColumnName = "IDPRESUPUESTO")
    @ManyToOne
    private Presupuesto idpresupuesto;

    public Compras() {
    }

    public Compras(String idequipos) {
        this.idequipos = idequipos;
    }

    public String getIdequipos() {
        return idequipos;
    }

    public void setIdequipos(String idequipos) {
        this.idequipos = idequipos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public BigInteger getValorunitario() {
        return valorunitario;
    }

    public void setValorunitario(BigInteger valorunitario) {
        this.valorunitario = valorunitario;
    }

    public BigInteger getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigInteger cantidad) {
        this.cantidad = cantidad;
    }

    public BigInteger getValortotal() {
        return valortotal;
    }

    public void setValortotal(BigInteger valortotal) {
        this.valortotal = valortotal;
    }

    public Character getEquipos() {
        return equipos;
    }

    public void setEquipos(Character equipos) {
        this.equipos = equipos;
    }

    public Character getSoftware() {
        return software;
    }

    public void setSoftware(Character software) {
        this.software = software;
    }

    public Character getMateriales() {
        return materiales;
    }

    public void setMateriales(Character materiales) {
        this.materiales = materiales;
    }

    public Character getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(Character bibliografia) {
        this.bibliografia = bibliografia;
    }

    public BigInteger getRubro1() {
        return rubro1;
    }

    public void setRubro1(BigInteger rubro1) {
        this.rubro1 = rubro1;
    }

    public BigInteger getRubro2() {
        return rubro2;
    }

    public void setRubro2(BigInteger rubro2) {
        this.rubro2 = rubro2;
    }

    public BigInteger getRubro3() {
        return rubro3;
    }

    public void setRubro3(BigInteger rubro3) {
        this.rubro3 = rubro3;
    }

    public BigInteger getRubro4() {
        return rubro4;
    }

    public void setRubro4(BigInteger rubro4) {
        this.rubro4 = rubro4;
    }

    public Presupuesto getIdpresupuesto() {
        return idpresupuesto;
    }

    public void setIdpresupuesto(Presupuesto idpresupuesto) {
        this.idpresupuesto = idpresupuesto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idequipos != null ? idequipos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compras)) {
            return false;
        }
        Compras other = (Compras) object;
        if ((this.idequipos == null && other.idequipos != null) || (this.idequipos != null && !this.idequipos.equals(other.idequipos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Compras[ idequipos=" + idequipos + " ]";
    }
    
}
