/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "ACTAINICIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actainicio.findAll", query = "SELECT a FROM Actainicio a"),
    @NamedQuery(name = "Actainicio.findByIdactainicio", query = "SELECT a FROM Actainicio a WHERE a.idactainicio = :idactainicio"),
    @NamedQuery(name = "Actainicio.findByFecha", query = "SELECT a FROM Actainicio a WHERE a.fecha = :fecha"),
    @NamedQuery(name = "Actainicio.findByActaaprobacionconsefacultad", query = "SELECT a FROM Actainicio a WHERE a.actaaprobacionconsefacultad = :actaaprobacionconsefacultad"),
    @NamedQuery(name = "Actainicio.findByCategoriacolciencias", query = "SELECT a FROM Actainicio a WHERE a.categoriacolciencias = :categoriacolciencias"),
    @NamedQuery(name = "Actainicio.findByCodigogrupolac", query = "SELECT a FROM Actainicio a WHERE a.codigogrupolac = :codigogrupolac"),
    @NamedQuery(name = "Actainicio.findByPrimerinforme", query = "SELECT a FROM Actainicio a WHERE a.primerinforme = :primerinforme"),
    @NamedQuery(name = "Actainicio.findBySegundoinforme", query = "SELECT a FROM Actainicio a WHERE a.segundoinforme = :segundoinforme"),
    @NamedQuery(name = "Actainicio.findByInformefinal", query = "SELECT a FROM Actainicio a WHERE a.informefinal = :informefinal"),
    @NamedQuery(name = "Actainicio.findByNumarticulosnac", query = "SELECT a FROM Actainicio a WHERE a.numarticulosnac = :numarticulosnac"),
    @NamedQuery(name = "Actainicio.findByNumarticulosint", query = "SELECT a FROM Actainicio a WHERE a.numarticulosint = :numarticulosint"),
    @NamedQuery(name = "Actainicio.findByNumeventos", query = "SELECT a FROM Actainicio a WHERE a.numeventos = :numeventos"),
    @NamedQuery(name = "Actainicio.findByCompromisootrasentidruta", query = "SELECT a FROM Actainicio a WHERE a.compromisootrasentidruta = :compromisootrasentidruta")})
public class Actainicio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDACTAINICIO")
    private String idactainicio;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "ACTAAPROBACIONCONSEFACULTAD")
    private BigInteger actaaprobacionconsefacultad;
    @Size(max = 20)
    @Column(name = "CATEGORIACOLCIENCIAS")
    private String categoriacolciencias;
    @Size(max = 15)
    @Column(name = "CODIGOGRUPOLAC")
    private String codigogrupolac;
    @Column(name = "PRIMERINFORME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date primerinforme;
    @Column(name = "SEGUNDOINFORME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segundoinforme;
    @Column(name = "INFORMEFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date informefinal;
    @Column(name = "NUMARTICULOSNAC")
    private BigInteger numarticulosnac;
    @Column(name = "NUMARTICULOSINT")
    private BigInteger numarticulosint;
    @Column(name = "NUMEVENTOS")
    private BigInteger numeventos;
    @Size(max = 100)
    @Column(name = "COMPROMISOOTRASENTIDRUTA")
    private String compromisootrasentidruta;
    @JoinTable(name = "ACTA_ORGA", joinColumns = {
        @JoinColumn(name = "IDACTAI", referencedColumnName = "IDACTAINICIO")}, inverseJoinColumns = {
        @JoinColumn(name = "IDORGA", referencedColumnName = "IDORGA")})
    @ManyToMany
    private Collection<Orgfinancieros> orgfinancierosCollection;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne
    private Proyecto idproyecto;

    public Actainicio() {
    }

    public Actainicio(String idactainicio) {
        this.idactainicio = idactainicio;
    }

    public String getIdactainicio() {
        return idactainicio;
    }

    public void setIdactainicio(String idactainicio) {
        this.idactainicio = idactainicio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getActaaprobacionconsefacultad() {
        return actaaprobacionconsefacultad;
    }

    public void setActaaprobacionconsefacultad(BigInteger actaaprobacionconsefacultad) {
        this.actaaprobacionconsefacultad = actaaprobacionconsefacultad;
    }

    public String getCategoriacolciencias() {
        return categoriacolciencias;
    }

    public void setCategoriacolciencias(String categoriacolciencias) {
        this.categoriacolciencias = categoriacolciencias;
    }

    public String getCodigogrupolac() {
        return codigogrupolac;
    }

    public void setCodigogrupolac(String codigogrupolac) {
        this.codigogrupolac = codigogrupolac;
    }

    public Date getPrimerinforme() {
        return primerinforme;
    }

    public void setPrimerinforme(Date primerinforme) {
        this.primerinforme = primerinforme;
    }

    public Date getSegundoinforme() {
        return segundoinforme;
    }

    public void setSegundoinforme(Date segundoinforme) {
        this.segundoinforme = segundoinforme;
    }

    public Date getInformefinal() {
        return informefinal;
    }

    public void setInformefinal(Date informefinal) {
        this.informefinal = informefinal;
    }

    public BigInteger getNumarticulosnac() {
        return numarticulosnac;
    }

    public void setNumarticulosnac(BigInteger numarticulosnac) {
        this.numarticulosnac = numarticulosnac;
    }

    public BigInteger getNumarticulosint() {
        return numarticulosint;
    }

    public void setNumarticulosint(BigInteger numarticulosint) {
        this.numarticulosint = numarticulosint;
    }

    public BigInteger getNumeventos() {
        return numeventos;
    }

    public void setNumeventos(BigInteger numeventos) {
        this.numeventos = numeventos;
    }

    public String getCompromisootrasentidruta() {
        return compromisootrasentidruta;
    }

    public void setCompromisootrasentidruta(String compromisootrasentidruta) {
        this.compromisootrasentidruta = compromisootrasentidruta;
    }

    @XmlTransient
    public Collection<Orgfinancieros> getOrgfinancierosCollection() {
        return orgfinancierosCollection;
    }

    public void setOrgfinancierosCollection(Collection<Orgfinancieros> orgfinancierosCollection) {
        this.orgfinancierosCollection = orgfinancierosCollection;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idactainicio != null ? idactainicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actainicio)) {
            return false;
        }
        Actainicio other = (Actainicio) object;
        if ((this.idactainicio == null && other.idactainicio != null) || (this.idactainicio != null && !this.idactainicio.equals(other.idactainicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Actainicio[ idactainicio=" + idactainicio + " ]";
    }
    
}
