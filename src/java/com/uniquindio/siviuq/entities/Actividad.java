/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "ACTIVIDAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actividad.findAll", query = "SELECT a FROM Actividad a"),
    @NamedQuery(name = "Actividad.findByIdactividad", query = "SELECT a FROM Actividad a WHERE a.idactividad = :idactividad"),
    @NamedQuery(name = "Actividad.findByNumero", query = "SELECT a FROM Actividad a WHERE a.numero = :numero"),
    @NamedQuery(name = "Actividad.findByDescripcion", query = "SELECT a FROM Actividad a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "Actividad.findByCantidadmeses", query = "SELECT a FROM Actividad a WHERE a.cantidadmeses = :cantidadmeses"),
    @NamedQuery(name = "Actividad.findByMesinicio", query = "SELECT a FROM Actividad a WHERE a.mesinicio = :mesinicio")})
public class Actividad implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDACTIVIDAD")
    private String idactividad;
    @Column(name = "NUMERO")
    private BigInteger numero;
    @Size(max = 300)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "CANTIDADMESES")
    private BigInteger cantidadmeses;
    @Column(name = "MESINICIO")
    private BigInteger mesinicio;
    @JoinColumn(name = "IDCRONOGRAMA", referencedColumnName = "IDCRONOGRAMA")
    @ManyToOne(optional = false)
    private Cronograma idcronograma;

    public Actividad() {
    }

    public Actividad(String idactividad) {
        this.idactividad = idactividad;
    }

    public String getIdactividad() {
        return idactividad;
    }

    public void setIdactividad(String idactividad) {
        this.idactividad = idactividad;
    }

    public BigInteger getNumero() {
        return numero;
    }

    public void setNumero(BigInteger numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getCantidadmeses() {
        return cantidadmeses;
    }

    public void setCantidadmeses(BigInteger cantidadmeses) {
        this.cantidadmeses = cantidadmeses;
    }

    public BigInteger getMesinicio() {
        return mesinicio;
    }

    public void setMesinicio(BigInteger mesinicio) {
        this.mesinicio = mesinicio;
    }

    public Cronograma getIdcronograma() {
        return idcronograma;
    }

    public void setIdcronograma(Cronograma idcronograma) {
        this.idcronograma = idcronograma;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idactividad != null ? idactividad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actividad)) {
            return false;
        }
        Actividad other = (Actividad) object;
        if ((this.idactividad == null && other.idactividad != null) || (this.idactividad != null && !this.idactividad.equals(other.idactividad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Actividad[ idactividad=" + idactividad + " ]";
    }
    
}
