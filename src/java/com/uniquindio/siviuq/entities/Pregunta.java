/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "PREGUNTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pregunta.findAll", query = "SELECT p FROM Pregunta p"),
    @NamedQuery(name = "Pregunta.findByIdpregunta", query = "SELECT p FROM Pregunta p WHERE p.idpregunta = :idpregunta"),
    @NamedQuery(name = "Pregunta.findByDescripcion", query = "SELECT p FROM Pregunta p WHERE p.descripcion = :descripcion"),
    @NamedQuery(name = "Pregunta.findBySi", query = "SELECT p FROM Pregunta p WHERE p.si = :si"),
    @NamedQuery(name = "Pregunta.findByNo", query = "SELECT p FROM Pregunta p WHERE p.no = :no"),
    @NamedQuery(name = "Pregunta.findByComentario", query = "SELECT p FROM Pregunta p WHERE p.comentario = :comentario")})
public class Pregunta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDPREGUNTA")
    private String idpregunta;
    @Size(max = 400)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "SI")
    private Character si;
    @Column(name = "NO")
    private Character no;
    @Size(max = 1000)
    @Column(name = "COMENTARIO")
    private String comentario;
    @JoinColumn(name = "IDINFORMEAVANCE", referencedColumnName = "IDINFAVA")
    @ManyToOne(optional = false)
    private Inforavan idinformeavance;

    public Pregunta() {
    }

    public Pregunta(String idpregunta) {
        this.idpregunta = idpregunta;
    }

    public String getIdpregunta() {
        return idpregunta;
    }

    public void setIdpregunta(String idpregunta) {
        this.idpregunta = idpregunta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getSi() {
        return si;
    }

    public void setSi(Character si) {
        this.si = si;
    }

    public Character getNo() {
        return no;
    }

    public void setNo(Character no) {
        this.no = no;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Inforavan getIdinformeavance() {
        return idinformeavance;
    }

    public void setIdinformeavance(Inforavan idinformeavance) {
        this.idinformeavance = idinformeavance;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpregunta != null ? idpregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pregunta)) {
            return false;
        }
        Pregunta other = (Pregunta) object;
        if ((this.idpregunta == null && other.idpregunta != null) || (this.idpregunta != null && !this.idpregunta.equals(other.idpregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Pregunta[ idpregunta=" + idpregunta + " ]";
    }
    
}
