/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "ESTUDIANTES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estudiantes.findAll", query = "SELECT e FROM Estudiantes e"),
    @NamedQuery(name = "Estudiantes.findByIdestud", query = "SELECT e FROM Estudiantes e WHERE e.idestud = :idestud"),
    @NamedQuery(name = "Estudiantes.findByNombre", query = "SELECT e FROM Estudiantes e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Estudiantes.findByApellidos", query = "SELECT e FROM Estudiantes e WHERE e.apellidos = :apellidos"),
    @NamedQuery(name = "Estudiantes.findByTelefonofijo", query = "SELECT e FROM Estudiantes e WHERE e.telefonofijo = :telefonofijo"),
    @NamedQuery(name = "Estudiantes.findByTelefonocelular", query = "SELECT e FROM Estudiantes e WHERE e.telefonocelular = :telefonocelular"),
    @NamedQuery(name = "Estudiantes.findByCorreo", query = "SELECT e FROM Estudiantes e WHERE e.correo = :correo"),
    @NamedQuery(name = "Estudiantes.findByEdad", query = "SELECT e FROM Estudiantes e WHERE e.edad = :edad"),
    @NamedQuery(name = "Estudiantes.findByDireccion", query = "SELECT e FROM Estudiantes e WHERE e.direccion = :direccion")})
public class Estudiantes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDESTUD")
    private String idestud;
    @Size(max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 50)
    @Column(name = "APELLIDOS")
    private String apellidos;
    @Size(max = 20)
    @Column(name = "TELEFONOFIJO")
    private String telefonofijo;
    @Size(max = 20)
    @Column(name = "TELEFONOCELULAR")
    private String telefonocelular;
    @Size(max = 50)
    @Column(name = "CORREO")
    private String correo;
    @Column(name = "EDAD")
    private BigInteger edad;
    @Size(max = 50)
    @Column(name = "DIRECCION")
    private String direccion;
    @JoinTable(name = "ESTUDIANTES_PROYECTO", joinColumns = {
        @JoinColumn(name = "IDESTU", referencedColumnName = "IDESTUD")}, inverseJoinColumns = {
        @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")})
    @ManyToMany
    private Collection<Proyecto> proyectoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idauxiliardeinvestigacion")
    private Collection<Infoauxi> infoauxiCollection;
    @JoinColumn(name = "IDGRUPOINV", referencedColumnName = "IDGINVES")
    @ManyToOne(optional = false)
    private Grupoinvestigacion idgrupoinv;
    @JoinColumn(name = "IDPROGRAMA", referencedColumnName = "IDPROGRAMA")
    @ManyToOne(optional = false)
    private Programa idprograma;

    public Estudiantes() {
    }

    public Estudiantes(String idestud) {
        this.idestud = idestud;
    }

    public String getIdestud() {
        return idestud;
    }

    public void setIdestud(String idestud) {
        this.idestud = idestud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefonofijo() {
        return telefonofijo;
    }

    public void setTelefonofijo(String telefonofijo) {
        this.telefonofijo = telefonofijo;
    }

    public String getTelefonocelular() {
        return telefonocelular;
    }

    public void setTelefonocelular(String telefonocelular) {
        this.telefonocelular = telefonocelular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public BigInteger getEdad() {
        return edad;
    }

    public void setEdad(BigInteger edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @XmlTransient
    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    @XmlTransient
    public Collection<Infoauxi> getInfoauxiCollection() {
        return infoauxiCollection;
    }

    public void setInfoauxiCollection(Collection<Infoauxi> infoauxiCollection) {
        this.infoauxiCollection = infoauxiCollection;
    }

    public Grupoinvestigacion getIdgrupoinv() {
        return idgrupoinv;
    }

    public void setIdgrupoinv(Grupoinvestigacion idgrupoinv) {
        this.idgrupoinv = idgrupoinv;
    }

    public Programa getIdprograma() {
        return idprograma;
    }

    public void setIdprograma(Programa idprograma) {
        this.idprograma = idprograma;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestud != null ? idestud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estudiantes)) {
            return false;
        }
        Estudiantes other = (Estudiantes) object;
        if ((this.idestud == null && other.idestud != null) || (this.idestud != null && !this.idestud.equals(other.idestud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Estudiantes[ idestud=" + idestud + " ]";
    }
    
}
