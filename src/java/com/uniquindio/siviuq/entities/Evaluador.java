/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "EVALUADOR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evaluador.findAll", query = "SELECT e FROM Evaluador e"),
    @NamedQuery(name = "Evaluador.findByIdeval", query = "SELECT e FROM Evaluador e WHERE e.ideval = :ideval"),
    @NamedQuery(name = "Evaluador.findByNombres", query = "SELECT e FROM Evaluador e WHERE e.nombres = :nombres"),
    @NamedQuery(name = "Evaluador.findByApellidos", query = "SELECT e FROM Evaluador e WHERE e.apellidos = :apellidos"),
    @NamedQuery(name = "Evaluador.findByRolenlauniversidad", query = "SELECT e FROM Evaluador e WHERE e.rolenlauniversidad = :rolenlauniversidad")})
public class Evaluador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDEVAL")
    private String ideval;
    @Size(max = 100)
    @Column(name = "NOMBRES")
    private String nombres;
    @Size(max = 100)
    @Column(name = "APELLIDOS")
    private String apellidos;
    @Size(max = 200)
    @Column(name = "ROLENLAUNIVERSIDAD")
    private String rolenlauniversidad;
    @JoinColumn(name = "IDFORMATOEVALUACION", referencedColumnName = "IDFOREVALU")
    @ManyToOne(optional = false)
    private Evproyectos idformatoevaluacion;

    public Evaluador() {
    }

    public Evaluador(String ideval) {
        this.ideval = ideval;
    }

    public String getIdeval() {
        return ideval;
    }

    public void setIdeval(String ideval) {
        this.ideval = ideval;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getRolenlauniversidad() {
        return rolenlauniversidad;
    }

    public void setRolenlauniversidad(String rolenlauniversidad) {
        this.rolenlauniversidad = rolenlauniversidad;
    }

    public Evproyectos getIdformatoevaluacion() {
        return idformatoevaluacion;
    }

    public void setIdformatoevaluacion(Evproyectos idformatoevaluacion) {
        this.idformatoevaluacion = idformatoevaluacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ideval != null ? ideval.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluador)) {
            return false;
        }
        Evaluador other = (Evaluador) object;
        if ((this.ideval == null && other.ideval != null) || (this.ideval != null && !this.ideval.equals(other.ideval))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Evaluador[ ideval=" + ideval + " ]";
    }
    
}
