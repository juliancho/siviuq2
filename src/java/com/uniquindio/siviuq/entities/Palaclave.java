/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "PALACLAVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Palaclave.findAll", query = "SELECT p FROM Palaclave p"),
    @NamedQuery(name = "Palaclave.findByIdpalabraclave", query = "SELECT p FROM Palaclave p WHERE p.idpalabraclave = :idpalabraclave"),
    @NamedQuery(name = "Palaclave.findByPalabra", query = "SELECT p FROM Palaclave p WHERE p.palabra = :palabra")})
public class Palaclave implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDPALABRACLAVE")
    private String idpalabraclave;
    @Size(max = 100)
    @Column(name = "PALABRA")
    private String palabra;
    @JoinTable(name = "PALA_PROY", joinColumns = {
        @JoinColumn(name = "IDPALABRA", referencedColumnName = "IDPALABRACLAVE")}, inverseJoinColumns = {
        @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")})
    @ManyToMany
    private Collection<Proyecto> proyectoCollection;

    public Palaclave() {
    }

    public Palaclave(String idpalabraclave) {
        this.idpalabraclave = idpalabraclave;
    }

    public String getIdpalabraclave() {
        return idpalabraclave;
    }

    public void setIdpalabraclave(String idpalabraclave) {
        this.idpalabraclave = idpalabraclave;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    @XmlTransient
    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpalabraclave != null ? idpalabraclave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Palaclave)) {
            return false;
        }
        Palaclave other = (Palaclave) object;
        if ((this.idpalabraclave == null && other.idpalabraclave != null) || (this.idpalabraclave != null && !this.idpalabraclave.equals(other.idpalabraclave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Palaclave[ idpalabraclave=" + idpalabraclave + " ]";
    }
    
}
