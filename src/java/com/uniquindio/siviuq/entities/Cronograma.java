/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "CRONOGRAMA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cronograma.findAll", query = "SELECT c FROM Cronograma c"),
    @NamedQuery(name = "Cronograma.findByIdcronograma", query = "SELECT c FROM Cronograma c WHERE c.idcronograma = :idcronograma"),
    @NamedQuery(name = "Cronograma.findByCantidadmeses", query = "SELECT c FROM Cronograma c WHERE c.cantidadmeses = :cantidadmeses"),
    @NamedQuery(name = "Cronograma.findByCantidadactividades", query = "SELECT c FROM Cronograma c WHERE c.cantidadactividades = :cantidadactividades")})
public class Cronograma implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDCRONOGRAMA")
    private String idcronograma;
    @Column(name = "CANTIDADMESES")
    private BigInteger cantidadmeses;
    @Column(name = "CANTIDADACTIVIDADES")
    private BigInteger cantidadactividades;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne(optional = false)
    private Proyecto idproyecto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcronograma")
    private Collection<Actividad> actividadCollection;

    public Cronograma() {
    }

    public Cronograma(String idcronograma) {
        this.idcronograma = idcronograma;
    }

    public String getIdcronograma() {
        return idcronograma;
    }

    public void setIdcronograma(String idcronograma) {
        this.idcronograma = idcronograma;
    }

    public BigInteger getCantidadmeses() {
        return cantidadmeses;
    }

    public void setCantidadmeses(BigInteger cantidadmeses) {
        this.cantidadmeses = cantidadmeses;
    }

    public BigInteger getCantidadactividades() {
        return cantidadactividades;
    }

    public void setCantidadactividades(BigInteger cantidadactividades) {
        this.cantidadactividades = cantidadactividades;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @XmlTransient
    public Collection<Actividad> getActividadCollection() {
        return actividadCollection;
    }

    public void setActividadCollection(Collection<Actividad> actividadCollection) {
        this.actividadCollection = actividadCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcronograma != null ? idcronograma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cronograma)) {
            return false;
        }
        Cronograma other = (Cronograma) object;
        if ((this.idcronograma == null && other.idcronograma != null) || (this.idcronograma != null && !this.idcronograma.equals(other.idcronograma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Cronograma[ idcronograma=" + idcronograma + " ]";
    }
    
}
