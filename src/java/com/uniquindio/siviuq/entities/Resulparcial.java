/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "RESULPARCIAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Resulparcial.findAll", query = "SELECT r FROM Resulparcial r"),
    @NamedQuery(name = "Resulparcial.findByIdresultadoparcial", query = "SELECT r FROM Resulparcial r WHERE r.idresultadoparcial = :idresultadoparcial"),
    @NamedQuery(name = "Resulparcial.findByComponntes", query = "SELECT r FROM Resulparcial r WHERE r.componntes = :componntes"),
    @NamedQuery(name = "Resulparcial.findByActividades", query = "SELECT r FROM Resulparcial r WHERE r.actividades = :actividades"),
    @NamedQuery(name = "Resulparcial.findByPorcentajeejecucion", query = "SELECT r FROM Resulparcial r WHERE r.porcentajeejecucion = :porcentajeejecucion")})
public class Resulparcial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDRESULTADOPARCIAL")
    private String idresultadoparcial;
    @Size(max = 1000)
    @Column(name = "COMPONNTES")
    private String componntes;
    @Size(max = 1000)
    @Column(name = "ACTIVIDADES")
    private String actividades;
    @Column(name = "PORCENTAJEEJECUCION")
    private BigInteger porcentajeejecucion;
    @JoinColumn(name = "IDINFOAVAN", referencedColumnName = "IDINFAVA")
    @ManyToOne(optional = false)
    private Inforavan idinfoavan;

    public Resulparcial() {
    }

    public Resulparcial(String idresultadoparcial) {
        this.idresultadoparcial = idresultadoparcial;
    }

    public String getIdresultadoparcial() {
        return idresultadoparcial;
    }

    public void setIdresultadoparcial(String idresultadoparcial) {
        this.idresultadoparcial = idresultadoparcial;
    }

    public String getComponntes() {
        return componntes;
    }

    public void setComponntes(String componntes) {
        this.componntes = componntes;
    }

    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public BigInteger getPorcentajeejecucion() {
        return porcentajeejecucion;
    }

    public void setPorcentajeejecucion(BigInteger porcentajeejecucion) {
        this.porcentajeejecucion = porcentajeejecucion;
    }

    public Inforavan getIdinfoavan() {
        return idinfoavan;
    }

    public void setIdinfoavan(Inforavan idinfoavan) {
        this.idinfoavan = idinfoavan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idresultadoparcial != null ? idresultadoparcial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resulparcial)) {
            return false;
        }
        Resulparcial other = (Resulparcial) object;
        if ((this.idresultadoparcial == null && other.idresultadoparcial != null) || (this.idresultadoparcial != null && !this.idresultadoparcial.equals(other.idresultadoparcial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Resulparcial[ idresultadoparcial=" + idresultadoparcial + " ]";
    }
    
}
