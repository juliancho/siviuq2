/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "TITULO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Titulo.findAll", query = "SELECT t FROM Titulo t"),
    @NamedQuery(name = "Titulo.findByIdtitulo", query = "SELECT t FROM Titulo t WHERE t.idtitulo = :idtitulo"),
    @NamedQuery(name = "Titulo.findByNombre", query = "SELECT t FROM Titulo t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "Titulo.findByPregrado", query = "SELECT t FROM Titulo t WHERE t.pregrado = :pregrado"),
    @NamedQuery(name = "Titulo.findByPostgrado", query = "SELECT t FROM Titulo t WHERE t.postgrado = :postgrado"),
    @NamedQuery(name = "Titulo.findByNumeroacta", query = "SELECT t FROM Titulo t WHERE t.numeroacta = :numeroacta"),
    @NamedQuery(name = "Titulo.findByFecha", query = "SELECT t FROM Titulo t WHERE t.fecha = :fecha"),
    @NamedQuery(name = "Titulo.findByTipopostgrado", query = "SELECT t FROM Titulo t WHERE t.tipopostgrado = :tipopostgrado")})
public class Titulo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDTITULO")
    private String idtitulo;
    @Size(max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "PREGRADO")
    private Character pregrado;
    @Column(name = "POSTGRADO")
    private Character postgrado;
    @Size(max = 10)
    @Column(name = "NUMEROACTA")
    private String numeroacta;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Size(max = 15)
    @Column(name = "TIPOPOSTGRADO")
    private String tipopostgrado;
    @JoinColumn(name = "IDINVESTIGADOR", referencedColumnName = "IDINVESTIGADOR")
    @ManyToOne(optional = false)
    private Investigador idinvestigador;

    public Titulo() {
    }

    public Titulo(String idtitulo) {
        this.idtitulo = idtitulo;
    }

    public String getIdtitulo() {
        return idtitulo;
    }

    public void setIdtitulo(String idtitulo) {
        this.idtitulo = idtitulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Character getPregrado() {
        return pregrado;
    }

    public void setPregrado(Character pregrado) {
        this.pregrado = pregrado;
    }

    public Character getPostgrado() {
        return postgrado;
    }

    public void setPostgrado(Character postgrado) {
        this.postgrado = postgrado;
    }

    public String getNumeroacta() {
        return numeroacta;
    }

    public void setNumeroacta(String numeroacta) {
        this.numeroacta = numeroacta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipopostgrado() {
        return tipopostgrado;
    }

    public void setTipopostgrado(String tipopostgrado) {
        this.tipopostgrado = tipopostgrado;
    }

    public Investigador getIdinvestigador() {
        return idinvestigador;
    }

    public void setIdinvestigador(Investigador idinvestigador) {
        this.idinvestigador = idinvestigador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtitulo != null ? idtitulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Titulo)) {
            return false;
        }
        Titulo other = (Titulo) object;
        if ((this.idtitulo == null && other.idtitulo != null) || (this.idtitulo != null && !this.idtitulo.equals(other.idtitulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Titulo[ idtitulo=" + idtitulo + " ]";
    }
    
}
