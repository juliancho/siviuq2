/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "ANEXO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Anexo.findAll", query = "SELECT a FROM Anexo a"),
    @NamedQuery(name = "Anexo.findByIdanexo", query = "SELECT a FROM Anexo a WHERE a.idanexo = :idanexo"),
    @NamedQuery(name = "Anexo.findByAnexoruta", query = "SELECT a FROM Anexo a WHERE a.anexoruta = :anexoruta")})
public class Anexo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDANEXO")
    private String idanexo;
    @Size(max = 200)
    @Column(name = "ANEXORUTA")
    private String anexoruta;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne
    private Proyecto idproyecto;

    public Anexo() {
    }

    public Anexo(String idanexo) {
        this.idanexo = idanexo;
    }

    public String getIdanexo() {
        return idanexo;
    }

    public void setIdanexo(String idanexo) {
        this.idanexo = idanexo;
    }

    public String getAnexoruta() {
        return anexoruta;
    }

    public void setAnexoruta(String anexoruta) {
        this.anexoruta = anexoruta;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idanexo != null ? idanexo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Anexo)) {
            return false;
        }
        Anexo other = (Anexo) object;
        if ((this.idanexo == null && other.idanexo != null) || (this.idanexo != null && !this.idanexo.equals(other.idanexo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Anexo[ idanexo=" + idanexo + " ]";
    }
    
}
