/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "EVPROYECTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evproyectos.findAll", query = "SELECT e FROM Evproyectos e"),
    @NamedQuery(name = "Evproyectos.findByIdforevalu", query = "SELECT e FROM Evproyectos e WHERE e.idforevalu = :idforevalu")})
public class Evproyectos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDFOREVALU")
    private String idforevalu;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idformatoevaluacionproyecto")
    private Collection<Valoracion> valoracionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idformatoevaluacion")
    private Collection<Evaluador> evaluadorCollection;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne
    private Proyecto idproyecto;

    public Evproyectos() {
    }

    public Evproyectos(String idforevalu) {
        this.idforevalu = idforevalu;
    }

    public String getIdforevalu() {
        return idforevalu;
    }

    public void setIdforevalu(String idforevalu) {
        this.idforevalu = idforevalu;
    }

    @XmlTransient
    public Collection<Valoracion> getValoracionCollection() {
        return valoracionCollection;
    }

    public void setValoracionCollection(Collection<Valoracion> valoracionCollection) {
        this.valoracionCollection = valoracionCollection;
    }

    @XmlTransient
    public Collection<Evaluador> getEvaluadorCollection() {
        return evaluadorCollection;
    }

    public void setEvaluadorCollection(Collection<Evaluador> evaluadorCollection) {
        this.evaluadorCollection = evaluadorCollection;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idforevalu != null ? idforevalu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evproyectos)) {
            return false;
        }
        Evproyectos other = (Evproyectos) object;
        if ((this.idforevalu == null && other.idforevalu != null) || (this.idforevalu != null && !this.idforevalu.equals(other.idforevalu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Evproyectos[ idforevalu=" + idforevalu + " ]";
    }
    
}
