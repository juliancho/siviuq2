/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "GRUPOINVESTIGACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupoinvestigacion.findAll", query = "SELECT g FROM Grupoinvestigacion g"),
    @NamedQuery(name = "Grupoinvestigacion.findByIdginves", query = "SELECT g FROM Grupoinvestigacion g WHERE g.idginves = :idginves"),
    @NamedQuery(name = "Grupoinvestigacion.findByNombre", query = "SELECT g FROM Grupoinvestigacion g WHERE g.nombre = :nombre")})
public class Grupoinvestigacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDGINVES")
    private String idginves;
    @Size(max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idgrupoinv")
    private Collection<Investigador> investigadorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idgrupoinv")
    private Collection<Estudiantes> estudiantesCollection;
    @JoinColumn(name = "IDPROGRAMA", referencedColumnName = "IDPROGRAMA")
    @ManyToOne(optional = false)
    private Programa idprograma;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idgrupoinvestigacion")
    private Collection<Proyecto> proyectoCollection;

    public Grupoinvestigacion() {
    }

    public Grupoinvestigacion(String idginves) {
        this.idginves = idginves;
    }

    public String getIdginves() {
        return idginves;
    }

    public void setIdginves(String idginves) {
        this.idginves = idginves;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<Investigador> getInvestigadorCollection() {
        return investigadorCollection;
    }

    public void setInvestigadorCollection(Collection<Investigador> investigadorCollection) {
        this.investigadorCollection = investigadorCollection;
    }

    @XmlTransient
    public Collection<Estudiantes> getEstudiantesCollection() {
        return estudiantesCollection;
    }

    public void setEstudiantesCollection(Collection<Estudiantes> estudiantesCollection) {
        this.estudiantesCollection = estudiantesCollection;
    }

    public Programa getIdprograma() {
        return idprograma;
    }

    public void setIdprograma(Programa idprograma) {
        this.idprograma = idprograma;
    }

    @XmlTransient
    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idginves != null ? idginves.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupoinvestigacion)) {
            return false;
        }
        Grupoinvestigacion other = (Grupoinvestigacion) object;
        if ((this.idginves == null && other.idginves != null) || (this.idginves != null && !this.idginves.equals(other.idginves))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Grupoinvestigacion[ idginves=" + idginves + " ]";
    }
    
}
