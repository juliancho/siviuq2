/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p"),
    @NamedQuery(name = "Proyecto.findByIdproyecto", query = "SELECT p FROM Proyecto p WHERE p.idproyecto = :idproyecto"),
    @NamedQuery(name = "Proyecto.findByTitulo", query = "SELECT p FROM Proyecto p WHERE p.titulo = :titulo"),
    @NamedQuery(name = "Proyecto.findByFecha", query = "SELECT p FROM Proyecto p WHERE p.fecha = :fecha"),
    @NamedQuery(name = "Proyecto.findByLineainves", query = "SELECT p FROM Proyecto p WHERE p.lineainves = :lineainves"),
    @NamedQuery(name = "Proyecto.findByTotalinves", query = "SELECT p FROM Proyecto p WHERE p.totalinves = :totalinves"),
    @NamedQuery(name = "Proyecto.findByEntidad", query = "SELECT p FROM Proyecto p WHERE p.entidad = :entidad"),
    @NamedQuery(name = "Proyecto.findByLugarejecucion", query = "SELECT p FROM Proyecto p WHERE p.lugarejecucion = :lugarejecucion"),
    @NamedQuery(name = "Proyecto.findByCiudad", query = "SELECT p FROM Proyecto p WHERE p.ciudad = :ciudad"),
    @NamedQuery(name = "Proyecto.findByDuracionmeses", query = "SELECT p FROM Proyecto p WHERE p.duracionmeses = :duracionmeses"),
    @NamedQuery(name = "Proyecto.findByTipoproyecto", query = "SELECT p FROM Proyecto p WHERE p.tipoproyecto = :tipoproyecto"),
    @NamedQuery(name = "Proyecto.findByModalidad", query = "SELECT p FROM Proyecto p WHERE p.modalidad = :modalidad"),
    @NamedQuery(name = "Proyecto.findByResumenruta", query = "SELECT p FROM Proyecto p WHERE p.resumenruta = :resumenruta"),
    @NamedQuery(name = "Proyecto.findByMarcoteoricoruta", query = "SELECT p FROM Proyecto p WHERE p.marcoteoricoruta = :marcoteoricoruta"),
    @NamedQuery(name = "Proyecto.findByImpactosocialruta", query = "SELECT p FROM Proyecto p WHERE p.impactosocialruta = :impactosocialruta"),
    @NamedQuery(name = "Proyecto.findByPlanteamientoproblemaruta", query = "SELECT p FROM Proyecto p WHERE p.planteamientoproblemaruta = :planteamientoproblemaruta"),
    @NamedQuery(name = "Proyecto.findByJustificacionruta", query = "SELECT p FROM Proyecto p WHERE p.justificacionruta = :justificacionruta"),
    @NamedQuery(name = "Proyecto.findByMetodologiaruta", query = "SELECT p FROM Proyecto p WHERE p.metodologiaruta = :metodologiaruta"),
    @NamedQuery(name = "Proyecto.findByResultadosesperadosruta", query = "SELECT p FROM Proyecto p WHERE p.resultadosesperadosruta = :resultadosesperadosruta"),
    @NamedQuery(name = "Proyecto.findByProductosesperadosruta", query = "SELECT p FROM Proyecto p WHERE p.productosesperadosruta = :productosesperadosruta"),
    @NamedQuery(name = "Proyecto.findByBibliografiaruta", query = "SELECT p FROM Proyecto p WHERE p.bibliografiaruta = :bibliografiaruta"),
    @NamedQuery(name = "Proyecto.findByTrayectoriagrupinvesruta", query = "SELECT p FROM Proyecto p WHERE p.trayectoriagrupinvesruta = :trayectoriagrupinvesruta"),
    @NamedQuery(name = "Proyecto.findByAspectobioeticoruta", query = "SELECT p FROM Proyecto p WHERE p.aspectobioeticoruta = :aspectobioeticoruta"),
    @NamedQuery(name = "Proyecto.findByUnidadejecutora", query = "SELECT p FROM Proyecto p WHERE p.unidadejecutora = :unidadejecutora"),
    @NamedQuery(name = "Proyecto.findByEntidadcofinanciadora", query = "SELECT p FROM Proyecto p WHERE p.entidadcofinanciadora = :entidadcofinanciadora"),
    @NamedQuery(name = "Proyecto.findByNumerocontrato", query = "SELECT p FROM Proyecto p WHERE p.numerocontrato = :numerocontrato"),
    @NamedQuery(name = "Proyecto.findByDuracionreal", query = "SELECT p FROM Proyecto p WHERE p.duracionreal = :duracionreal"),
    @NamedQuery(name = "Proyecto.findByResumenresultadosruta", query = "SELECT p FROM Proyecto p WHERE p.resumenresultadosruta = :resumenresultadosruta"),
    @NamedQuery(name = "Proyecto.findByInformecientificoruta", query = "SELECT p FROM Proyecto p WHERE p.informecientificoruta = :informecientificoruta"),
    @NamedQuery(name = "Proyecto.findByResultadodirectoruta", query = "SELECT p FROM Proyecto p WHERE p.resultadodirectoruta = :resultadodirectoruta"),
    @NamedQuery(name = "Proyecto.findByResultadoindirectoruta", query = "SELECT p FROM Proyecto p WHERE p.resultadoindirectoruta = :resultadoindirectoruta"),
    @NamedQuery(name = "Proyecto.findByLimitantesruta", query = "SELECT p FROM Proyecto p WHERE p.limitantesruta = :limitantesruta")})
public class Proyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDPROYECTO")
    private String idproyecto;
    @Size(max = 100)
    @Column(name = "TITULO")
    private String titulo;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Size(max = 100)
    @Column(name = "LINEAINVES")
    private String lineainves;
    @Column(name = "TOTALINVES")
    private BigInteger totalinves;
    @Size(max = 100)
    @Column(name = "ENTIDAD")
    private String entidad;
    @Size(max = 100)
    @Column(name = "LUGAREJECUCION")
    private String lugarejecucion;
    @Size(max = 100)
    @Column(name = "CIUDAD")
    private String ciudad;
    @Column(name = "DURACIONMESES")
    private BigInteger duracionmeses;
    @Size(max = 100)
    @Column(name = "TIPOPROYECTO")
    private String tipoproyecto;
    @Size(max = 100)
    @Column(name = "MODALIDAD")
    private String modalidad;
    @Size(max = 200)
    @Column(name = "RESUMENRUTA")
    private String resumenruta;
    @Size(max = 200)
    @Column(name = "MARCOTEORICORUTA")
    private String marcoteoricoruta;
    @Size(max = 200)
    @Column(name = "IMPACTOSOCIALRUTA")
    private String impactosocialruta;
    @Size(max = 200)
    @Column(name = "PLANTEAMIENTOPROBLEMARUTA")
    private String planteamientoproblemaruta;
    @Size(max = 200)
    @Column(name = "JUSTIFICACIONRUTA")
    private String justificacionruta;
    @Size(max = 200)
    @Column(name = "METODOLOGIARUTA")
    private String metodologiaruta;
    @Size(max = 200)
    @Column(name = "RESULTADOSESPERADOSRUTA")
    private String resultadosesperadosruta;
    @Size(max = 200)
    @Column(name = "PRODUCTOSESPERADOSRUTA")
    private String productosesperadosruta;
    @Size(max = 200)
    @Column(name = "BIBLIOGRAFIARUTA")
    private String bibliografiaruta;
    @Size(max = 200)
    @Column(name = "TRAYECTORIAGRUPINVESRUTA")
    private String trayectoriagrupinvesruta;
    @Size(min = 1, max = 200)
    @Column(name = "ASPECTOBIOETICORUTA")
    private String aspectobioeticoruta;
    @Size(max = 200)
    @Column(name = "UNIDADEJECUTORA")
    private String unidadejecutora;
    @Size(max = 200)
    @Column(name = "ENTIDADCOFINANCIADORA")
    private String entidadcofinanciadora;
    @Size(max = 100)
    @Column(name = "NUMEROCONTRATO")
    private String numerocontrato;
    @Column(name = "DURACIONREAL")
    private BigInteger duracionreal;
    @Size(max = 200)
    @Column(name = "RESUMENRESULTADOSRUTA")
    private String resumenresultadosruta;
    @Size(max = 200)
    @Column(name = "INFORMECIENTIFICORUTA")
    private String informecientificoruta;
    @Size(max = 200)
    @Column(name = "RESULTADODIRECTORUTA")
    private String resultadodirectoruta;
    @Size(max = 200)
    @Column(name = "RESULTADOINDIRECTORUTA")
    private String resultadoindirectoruta;
    @Size(max = 200)
    @Column(name = "LIMITANTESRUTA")
    private String limitantesruta;
    @ManyToMany(mappedBy = "proyectoCollection")
    private Collection<Estudiantes> estudiantesCollection;
    @JoinTable(name = "INVESTIGADOR_PROYECTO", joinColumns = {
        @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")}, inverseJoinColumns = {
        @JoinColumn(name = "IDINVESTIGADOR", referencedColumnName = "IDINVESTIGADOR")})
    @ManyToMany
    private Collection<Investigador> investigadorCollection;
    @ManyToMany(mappedBy = "proyectoCollection")
    private Collection<Palaclave> palaclaveCollection;
    @ManyToMany(mappedBy = "proyectoCollection")
    private Collection<Convocatoria> convocatoriaCollection;
    @OneToMany(mappedBy = "idproyecto")
    private Collection<Inforavan> inforavanCollection;
    @OneToMany(mappedBy = "idproyecto")
    private Collection<Correccion> correccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproyecto")
    private Collection<Cronograma> cronogramaCollection;
    @OneToMany(mappedBy = "idproyecto")
    private Collection<Actainicio> actainicioCollection;
    @OneToMany(mappedBy = "idproyecto")
    private Collection<Actas> actasCollection;
    @OneToMany(mappedBy = "idproyecto")
    private Collection<Anexo> anexoCollection;
    @OneToMany(mappedBy = "idproyecto")
    private Collection<Oficio> oficioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproyecto")
    private Collection<Presupuesto> presupuestoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproyecto")
    private Collection<Objetivo> objetivoCollection;
    @OneToMany(mappedBy = "idproyecto")
    private Collection<Evproyectos> evproyectosCollection;
    @JoinColumn(name = "IDEVALUEXTER", referencedColumnName = "IDEVALUADOREXTERNO")
    @ManyToOne
    private Evarexter idevaluexter;
    @JoinColumn(name = "IDGRUPOINVESTIGACION", referencedColumnName = "IDGINVES")
    @ManyToOne(optional = false)
    private Grupoinvestigacion idgrupoinvestigacion;

    public Proyecto() {
    }

    public Proyecto(String idproyecto) {
        this.idproyecto = idproyecto;
    }

    public Proyecto(String idproyecto, String aspectobioeticoruta) {
        this.idproyecto = idproyecto;
        this.aspectobioeticoruta = aspectobioeticoruta;
    }

    public String getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(String idproyecto) {
        this.idproyecto = idproyecto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getLineainves() {
        return lineainves;
    }

    public void setLineainves(String lineainves) {
        this.lineainves = lineainves;
    }

    public BigInteger getTotalinves() {
        return totalinves;
    }

    public void setTotalinves(BigInteger totalinves) {
        this.totalinves = totalinves;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getLugarejecucion() {
        return lugarejecucion;
    }

    public void setLugarejecucion(String lugarejecucion) {
        this.lugarejecucion = lugarejecucion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public BigInteger getDuracionmeses() {
        return duracionmeses;
    }

    public void setDuracionmeses(BigInteger duracionmeses) {
        this.duracionmeses = duracionmeses;
    }

    public String getTipoproyecto() {
        return tipoproyecto;
    }

    public void setTipoproyecto(String tipoproyecto) {
        this.tipoproyecto = tipoproyecto;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getResumenruta() {
        return resumenruta;
    }

    public void setResumenruta(String resumenruta) {
        this.resumenruta = resumenruta;
    }

    public String getMarcoteoricoruta() {
        return marcoteoricoruta;
    }

    public void setMarcoteoricoruta(String marcoteoricoruta) {
        this.marcoteoricoruta = marcoteoricoruta;
    }

    public String getImpactosocialruta() {
        return impactosocialruta;
    }

    public void setImpactosocialruta(String impactosocialruta) {
        this.impactosocialruta = impactosocialruta;
    }

    public String getPlanteamientoproblemaruta() {
        return planteamientoproblemaruta;
    }

    public void setPlanteamientoproblemaruta(String planteamientoproblemaruta) {
        this.planteamientoproblemaruta = planteamientoproblemaruta;
    }

    public String getJustificacionruta() {
        return justificacionruta;
    }

    public void setJustificacionruta(String justificacionruta) {
        this.justificacionruta = justificacionruta;
    }

    public String getMetodologiaruta() {
        return metodologiaruta;
    }

    public void setMetodologiaruta(String metodologiaruta) {
        this.metodologiaruta = metodologiaruta;
    }

    public String getResultadosesperadosruta() {
        return resultadosesperadosruta;
    }

    public void setResultadosesperadosruta(String resultadosesperadosruta) {
        this.resultadosesperadosruta = resultadosesperadosruta;
    }

    public String getProductosesperadosruta() {
        return productosesperadosruta;
    }

    public void setProductosesperadosruta(String productosesperadosruta) {
        this.productosesperadosruta = productosesperadosruta;
    }

    public String getBibliografiaruta() {
        return bibliografiaruta;
    }

    public void setBibliografiaruta(String bibliografiaruta) {
        this.bibliografiaruta = bibliografiaruta;
    }

    public String getTrayectoriagrupinvesruta() {
        return trayectoriagrupinvesruta;
    }

    public void setTrayectoriagrupinvesruta(String trayectoriagrupinvesruta) {
        this.trayectoriagrupinvesruta = trayectoriagrupinvesruta;
    }

    public String getAspectobioeticoruta() {
        return aspectobioeticoruta;
    }

    public void setAspectobioeticoruta(String aspectobioeticoruta) {
        this.aspectobioeticoruta = aspectobioeticoruta;
    }

    public String getUnidadejecutora() {
        return unidadejecutora;
    }

    public void setUnidadejecutora(String unidadejecutora) {
        this.unidadejecutora = unidadejecutora;
    }

    public String getEntidadcofinanciadora() {
        return entidadcofinanciadora;
    }

    public void setEntidadcofinanciadora(String entidadcofinanciadora) {
        this.entidadcofinanciadora = entidadcofinanciadora;
    }

    public String getNumerocontrato() {
        return numerocontrato;
    }

    public void setNumerocontrato(String numerocontrato) {
        this.numerocontrato = numerocontrato;
    }

    public BigInteger getDuracionreal() {
        return duracionreal;
    }

    public void setDuracionreal(BigInteger duracionreal) {
        this.duracionreal = duracionreal;
    }

    public String getResumenresultadosruta() {
        return resumenresultadosruta;
    }

    public void setResumenresultadosruta(String resumenresultadosruta) {
        this.resumenresultadosruta = resumenresultadosruta;
    }

    public String getInformecientificoruta() {
        return informecientificoruta;
    }

    public void setInformecientificoruta(String informecientificoruta) {
        this.informecientificoruta = informecientificoruta;
    }

    public String getResultadodirectoruta() {
        return resultadodirectoruta;
    }

    public void setResultadodirectoruta(String resultadodirectoruta) {
        this.resultadodirectoruta = resultadodirectoruta;
    }

    public String getResultadoindirectoruta() {
        return resultadoindirectoruta;
    }

    public void setResultadoindirectoruta(String resultadoindirectoruta) {
        this.resultadoindirectoruta = resultadoindirectoruta;
    }

    public String getLimitantesruta() {
        return limitantesruta;
    }

    public void setLimitantesruta(String limitantesruta) {
        this.limitantesruta = limitantesruta;
    }

    @XmlTransient
    public Collection<Estudiantes> getEstudiantesCollection() {
        return estudiantesCollection;
    }

    public void setEstudiantesCollection(Collection<Estudiantes> estudiantesCollection) {
        this.estudiantesCollection = estudiantesCollection;
    }

    @XmlTransient
    public Collection<Investigador> getInvestigadorCollection() {
        return investigadorCollection;
    }

    public void setInvestigadorCollection(Collection<Investigador> investigadorCollection) {
        this.investigadorCollection = investigadorCollection;
    }

    @XmlTransient
    public Collection<Palaclave> getPalaclaveCollection() {
        return palaclaveCollection;
    }

    public void setPalaclaveCollection(Collection<Palaclave> palaclaveCollection) {
        this.palaclaveCollection = palaclaveCollection;
    }

    @XmlTransient
    public Collection<Convocatoria> getConvocatoriaCollection() {
        return convocatoriaCollection;
    }

    public void setConvocatoriaCollection(Collection<Convocatoria> convocatoriaCollection) {
        this.convocatoriaCollection = convocatoriaCollection;
    }

    @XmlTransient
    public Collection<Inforavan> getInforavanCollection() {
        return inforavanCollection;
    }

    public void setInforavanCollection(Collection<Inforavan> inforavanCollection) {
        this.inforavanCollection = inforavanCollection;
    }

    @XmlTransient
    public Collection<Correccion> getCorreccionCollection() {
        return correccionCollection;
    }

    public void setCorreccionCollection(Collection<Correccion> correccionCollection) {
        this.correccionCollection = correccionCollection;
    }

    @XmlTransient
    public Collection<Cronograma> getCronogramaCollection() {
        return cronogramaCollection;
    }

    public void setCronogramaCollection(Collection<Cronograma> cronogramaCollection) {
        this.cronogramaCollection = cronogramaCollection;
    }

    @XmlTransient
    public Collection<Actainicio> getActainicioCollection() {
        return actainicioCollection;
    }

    public void setActainicioCollection(Collection<Actainicio> actainicioCollection) {
        this.actainicioCollection = actainicioCollection;
    }

    @XmlTransient
    public Collection<Actas> getActasCollection() {
        return actasCollection;
    }

    public void setActasCollection(Collection<Actas> actasCollection) {
        this.actasCollection = actasCollection;
    }

    @XmlTransient
    public Collection<Anexo> getAnexoCollection() {
        return anexoCollection;
    }

    public void setAnexoCollection(Collection<Anexo> anexoCollection) {
        this.anexoCollection = anexoCollection;
    }

    @XmlTransient
    public Collection<Oficio> getOficioCollection() {
        return oficioCollection;
    }

    public void setOficioCollection(Collection<Oficio> oficioCollection) {
        this.oficioCollection = oficioCollection;
    }

    @XmlTransient
    public Collection<Presupuesto> getPresupuestoCollection() {
        return presupuestoCollection;
    }

    public void setPresupuestoCollection(Collection<Presupuesto> presupuestoCollection) {
        this.presupuestoCollection = presupuestoCollection;
    }

    @XmlTransient
    public Collection<Objetivo> getObjetivoCollection() {
        return objetivoCollection;
    }

    public void setObjetivoCollection(Collection<Objetivo> objetivoCollection) {
        this.objetivoCollection = objetivoCollection;
    }

    @XmlTransient
    public Collection<Evproyectos> getEvproyectosCollection() {
        return evproyectosCollection;
    }

    public void setEvproyectosCollection(Collection<Evproyectos> evproyectosCollection) {
        this.evproyectosCollection = evproyectosCollection;
    }

    public Evarexter getIdevaluexter() {
        return idevaluexter;
    }

    public void setIdevaluexter(Evarexter idevaluexter) {
        this.idevaluexter = idevaluexter;
    }

    public Grupoinvestigacion getIdgrupoinvestigacion() {
        return idgrupoinvestigacion;
    }

    public void setIdgrupoinvestigacion(Grupoinvestigacion idgrupoinvestigacion) {
        this.idgrupoinvestigacion = idgrupoinvestigacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproyecto != null ? idproyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.idproyecto == null && other.idproyecto != null) || (this.idproyecto != null && !this.idproyecto.equals(other.idproyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Proyecto[ idproyecto=" + idproyecto + " ]";
    }
    
}
