/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "INFOAUXI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Infoauxi.findAll", query = "SELECT i FROM Infoauxi i"),
    @NamedQuery(name = "Infoauxi.findByIdinfaux", query = "SELECT i FROM Infoauxi i WHERE i.idinfaux = :idinfaux"),
    @NamedQuery(name = "Infoauxi.findByActividadesrealizadas", query = "SELECT i FROM Infoauxi i WHERE i.actividadesrealizadas = :actividadesrealizadas")})
public class Infoauxi implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDINFAUX")
    private String idinfaux;
    @Size(max = 1000)
    @Column(name = "ACTIVIDADESREALIZADAS")
    private String actividadesrealizadas;
    @JoinColumn(name = "IDAUXILIARDEINVESTIGACION", referencedColumnName = "IDESTUD")
    @ManyToOne(optional = false)
    private Estudiantes idauxiliardeinvestigacion;
    @JoinColumn(name = "IDINFOAVAN", referencedColumnName = "IDINFAVA")
    @ManyToOne(optional = false)
    private Inforavan idinfoavan;

    public Infoauxi() {
    }

    public Infoauxi(String idinfaux) {
        this.idinfaux = idinfaux;
    }

    public String getIdinfaux() {
        return idinfaux;
    }

    public void setIdinfaux(String idinfaux) {
        this.idinfaux = idinfaux;
    }

    public String getActividadesrealizadas() {
        return actividadesrealizadas;
    }

    public void setActividadesrealizadas(String actividadesrealizadas) {
        this.actividadesrealizadas = actividadesrealizadas;
    }

    public Estudiantes getIdauxiliardeinvestigacion() {
        return idauxiliardeinvestigacion;
    }

    public void setIdauxiliardeinvestigacion(Estudiantes idauxiliardeinvestigacion) {
        this.idauxiliardeinvestigacion = idauxiliardeinvestigacion;
    }

    public Inforavan getIdinfoavan() {
        return idinfoavan;
    }

    public void setIdinfoavan(Inforavan idinfoavan) {
        this.idinfoavan = idinfoavan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinfaux != null ? idinfaux.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Infoauxi)) {
            return false;
        }
        Infoauxi other = (Infoauxi) object;
        if ((this.idinfaux == null && other.idinfaux != null) || (this.idinfaux != null && !this.idinfaux.equals(other.idinfaux))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Infoauxi[ idinfaux=" + idinfaux + " ]";
    }
    
}
