/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "ACTIVIMETAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Activimetas.findAll", query = "SELECT a FROM Activimetas a"),
    @NamedQuery(name = "Activimetas.findByIdactmetas", query = "SELECT a FROM Activimetas a WHERE a.idactmetas = :idactmetas"),
    @NamedQuery(name = "Activimetas.findByMetaprogramada", query = "SELECT a FROM Activimetas a WHERE a.metaprogramada = :metaprogramada"),
    @NamedQuery(name = "Activimetas.findByMetarealizada", query = "SELECT a FROM Activimetas a WHERE a.metarealizada = :metarealizada"),
    @NamedQuery(name = "Activimetas.findByPorcentajecumplimiento", query = "SELECT a FROM Activimetas a WHERE a.porcentajecumplimiento = :porcentajecumplimiento")})
public class Activimetas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDACTMETAS")
    private String idactmetas;
    @Size(max = 1000)
    @Column(name = "METAPROGRAMADA")
    private String metaprogramada;
    @Size(max = 1000)
    @Column(name = "METAREALIZADA")
    private String metarealizada;
    @Column(name = "PORCENTAJECUMPLIMIENTO")
    private BigInteger porcentajecumplimiento;
    @JoinColumn(name = "IDINFOAVAN", referencedColumnName = "IDINFAVA")
    @ManyToOne(optional = false)
    private Inforavan idinfoavan;

    public Activimetas() {
    }

    public Activimetas(String idactmetas) {
        this.idactmetas = idactmetas;
    }

    public String getIdactmetas() {
        return idactmetas;
    }

    public void setIdactmetas(String idactmetas) {
        this.idactmetas = idactmetas;
    }

    public String getMetaprogramada() {
        return metaprogramada;
    }

    public void setMetaprogramada(String metaprogramada) {
        this.metaprogramada = metaprogramada;
    }

    public String getMetarealizada() {
        return metarealizada;
    }

    public void setMetarealizada(String metarealizada) {
        this.metarealizada = metarealizada;
    }

    public BigInteger getPorcentajecumplimiento() {
        return porcentajecumplimiento;
    }

    public void setPorcentajecumplimiento(BigInteger porcentajecumplimiento) {
        this.porcentajecumplimiento = porcentajecumplimiento;
    }

    public Inforavan getIdinfoavan() {
        return idinfoavan;
    }

    public void setIdinfoavan(Inforavan idinfoavan) {
        this.idinfoavan = idinfoavan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idactmetas != null ? idactmetas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Activimetas)) {
            return false;
        }
        Activimetas other = (Activimetas) object;
        if ((this.idactmetas == null && other.idactmetas != null) || (this.idactmetas != null && !this.idactmetas.equals(other.idactmetas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Activimetas[ idactmetas=" + idactmetas + " ]";
    }
    
}
