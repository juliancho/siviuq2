/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "PRESUPUESTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Presupuesto.findAll", query = "SELECT p FROM Presupuesto p"),
    @NamedQuery(name = "Presupuesto.findByIdpresupuesto", query = "SELECT p FROM Presupuesto p WHERE p.idpresupuesto = :idpresupuesto"),
    @NamedQuery(name = "Presupuesto.findByEspecierecurrecte", query = "SELECT p FROM Presupuesto p WHERE p.especierecurrecte = :especierecurrecte"),
    @NamedQuery(name = "Presupuesto.findByTotal", query = "SELECT p FROM Presupuesto p WHERE p.total = :total")})
public class Presupuesto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDPRESUPUESTO")
    private String idpresupuesto;
    @Size(max = 10)
    @Column(name = "ESPECIERECURRECTE")
    private String especierecurrecte;
    @Column(name = "TOTAL")
    private BigInteger total;
    @OneToMany(mappedBy = "idpresupuesto")
    private Collection<Gastospersonal> gastospersonalCollection;
    @OneToMany(mappedBy = "idcuadropresupuesto")
    private Collection<Viajesalida> viajesalidaCollection;
    @OneToMany(mappedBy = "idpresupuesto")
    private Collection<Compras> comprasCollection;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne(optional = false)
    private Proyecto idproyecto;

    public Presupuesto() {
    }

    public Presupuesto(String idpresupuesto) {
        this.idpresupuesto = idpresupuesto;
    }

    public String getIdpresupuesto() {
        return idpresupuesto;
    }

    public void setIdpresupuesto(String idpresupuesto) {
        this.idpresupuesto = idpresupuesto;
    }

    public String getEspecierecurrecte() {
        return especierecurrecte;
    }

    public void setEspecierecurrecte(String especierecurrecte) {
        this.especierecurrecte = especierecurrecte;
    }

    public BigInteger getTotal() {
        return total;
    }

    public void setTotal(BigInteger total) {
        this.total = total;
    }

    @XmlTransient
    public Collection<Gastospersonal> getGastospersonalCollection() {
        return gastospersonalCollection;
    }

    public void setGastospersonalCollection(Collection<Gastospersonal> gastospersonalCollection) {
        this.gastospersonalCollection = gastospersonalCollection;
    }

    @XmlTransient
    public Collection<Viajesalida> getViajesalidaCollection() {
        return viajesalidaCollection;
    }

    public void setViajesalidaCollection(Collection<Viajesalida> viajesalidaCollection) {
        this.viajesalidaCollection = viajesalidaCollection;
    }

    @XmlTransient
    public Collection<Compras> getComprasCollection() {
        return comprasCollection;
    }

    public void setComprasCollection(Collection<Compras> comprasCollection) {
        this.comprasCollection = comprasCollection;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpresupuesto != null ? idpresupuesto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Presupuesto)) {
            return false;
        }
        Presupuesto other = (Presupuesto) object;
        if ((this.idpresupuesto == null && other.idpresupuesto != null) || (this.idpresupuesto != null && !this.idpresupuesto.equals(other.idpresupuesto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Presupuesto[ idpresupuesto=" + idpresupuesto + " ]";
    }
    
}
