/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "ACTAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actas.findAll", query = "SELECT a FROM Actas a"),
    @NamedQuery(name = "Actas.findByIdactas", query = "SELECT a FROM Actas a WHERE a.idactas = :idactas"),
    @NamedQuery(name = "Actas.findByUrl", query = "SELECT a FROM Actas a WHERE a.url = :url"),
    @NamedQuery(name = "Actas.findByNumeroacta", query = "SELECT a FROM Actas a WHERE a.numeroacta = :numeroacta"),
    @NamedQuery(name = "Actas.findByNombre", query = "SELECT a FROM Actas a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Actas.findByEmisor", query = "SELECT a FROM Actas a WHERE a.emisor = :emisor")})
public class Actas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDACTAS")
    private String idactas;
    @Size(max = 100)
    @Column(name = "URL")
    private String url;
    @Column(name = "NUMEROACTA")
    private BigInteger numeroacta;
    @Size(max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 100)
    @Column(name = "EMISOR")
    private String emisor;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne
    private Proyecto idproyecto;

    public Actas() {
    }

    public Actas(String idactas) {
        this.idactas = idactas;
    }

    public String getIdactas() {
        return idactas;
    }

    public void setIdactas(String idactas) {
        this.idactas = idactas;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public BigInteger getNumeroacta() {
        return numeroacta;
    }

    public void setNumeroacta(BigInteger numeroacta) {
        this.numeroacta = numeroacta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idactas != null ? idactas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actas)) {
            return false;
        }
        Actas other = (Actas) object;
        if ((this.idactas == null && other.idactas != null) || (this.idactas != null && !this.idactas.equals(other.idactas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Actas[ idactas=" + idactas + " ]";
    }
    
}
