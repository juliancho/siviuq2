/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "INFOFINAN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Infofinan.findAll", query = "SELECT i FROM Infofinan i"),
    @NamedQuery(name = "Infofinan.findByIdinffin", query = "SELECT i FROM Infofinan i WHERE i.idinffin = :idinffin"),
    @NamedQuery(name = "Infofinan.findByActividad", query = "SELECT i FROM Infofinan i WHERE i.actividad = :actividad"),
    @NamedQuery(name = "Infofinan.findByPresuproyectado", query = "SELECT i FROM Infofinan i WHERE i.presuproyectado = :presuproyectado"),
    @NamedQuery(name = "Infofinan.findByPresuejecutado", query = "SELECT i FROM Infofinan i WHERE i.presuejecutado = :presuejecutado"),
    @NamedQuery(name = "Infofinan.findByTrimestre", query = "SELECT i FROM Infofinan i WHERE i.trimestre = :trimestre")})
public class Infofinan implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDINFFIN")
    private BigDecimal idinffin;
    @Size(max = 500)
    @Column(name = "ACTIVIDAD")
    private String actividad;
    @Column(name = "PRESUPROYECTADO")
    private BigInteger presuproyectado;
    @Column(name = "PRESUEJECUTADO")
    private BigInteger presuejecutado;
    @Column(name = "TRIMESTRE")
    private BigInteger trimestre;
    @JoinColumn(name = "IDINFOAVAN", referencedColumnName = "IDINFAVA")
    @ManyToOne(optional = false)
    private Inforavan idinfoavan;

    public Infofinan() {
    }

    public Infofinan(BigDecimal idinffin) {
        this.idinffin = idinffin;
    }

    public BigDecimal getIdinffin() {
        return idinffin;
    }

    public void setIdinffin(BigDecimal idinffin) {
        this.idinffin = idinffin;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public BigInteger getPresuproyectado() {
        return presuproyectado;
    }

    public void setPresuproyectado(BigInteger presuproyectado) {
        this.presuproyectado = presuproyectado;
    }

    public BigInteger getPresuejecutado() {
        return presuejecutado;
    }

    public void setPresuejecutado(BigInteger presuejecutado) {
        this.presuejecutado = presuejecutado;
    }

    public BigInteger getTrimestre() {
        return trimestre;
    }

    public void setTrimestre(BigInteger trimestre) {
        this.trimestre = trimestre;
    }

    public Inforavan getIdinfoavan() {
        return idinfoavan;
    }

    public void setIdinfoavan(Inforavan idinfoavan) {
        this.idinfoavan = idinfoavan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinffin != null ? idinffin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Infofinan)) {
            return false;
        }
        Infofinan other = (Infofinan) object;
        if ((this.idinffin == null && other.idinffin != null) || (this.idinffin != null && !this.idinffin.equals(other.idinffin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Infofinan[ idinffin=" + idinffin + " ]";
    }
    
}
