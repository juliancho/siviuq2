/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juliancho
 */
@Entity
@Table(name = "VALORACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Valoracion.findAll", query = "SELECT v FROM Valoracion v"),
    @NamedQuery(name = "Valoracion.findByIdevalor", query = "SELECT v FROM Valoracion v WHERE v.idevalor = :idevalor"),
    @NamedQuery(name = "Valoracion.findByDescripcion", query = "SELECT v FROM Valoracion v WHERE v.descripcion = :descripcion"),
    @NamedQuery(name = "Valoracion.findByValoracion", query = "SELECT v FROM Valoracion v WHERE v.valoracion = :valoracion"),
    @NamedQuery(name = "Valoracion.findByJustificacion", query = "SELECT v FROM Valoracion v WHERE v.justificacion = :justificacion")})
public class Valoracion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDEVALOR")
    private String idevalor;
    @Size(max = 1000)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "VALORACION")
    private BigInteger valoracion;
    @Size(max = 2000)
    @Column(name = "JUSTIFICACION")
    private String justificacion;
    @JoinColumn(name = "IDFORMATOEVALUACIONPROYECTO", referencedColumnName = "IDFOREVALU")
    @ManyToOne(optional = false)
    private Evproyectos idformatoevaluacionproyecto;

    public Valoracion() {
    }

    public Valoracion(String idevalor) {
        this.idevalor = idevalor;
    }

    public String getIdevalor() {
        return idevalor;
    }

    public void setIdevalor(String idevalor) {
        this.idevalor = idevalor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getValoracion() {
        return valoracion;
    }

    public void setValoracion(BigInteger valoracion) {
        this.valoracion = valoracion;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public Evproyectos getIdformatoevaluacionproyecto() {
        return idformatoevaluacionproyecto;
    }

    public void setIdformatoevaluacionproyecto(Evproyectos idformatoevaluacionproyecto) {
        this.idformatoevaluacionproyecto = idformatoevaluacionproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idevalor != null ? idevalor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Valoracion)) {
            return false;
        }
        Valoracion other = (Valoracion) object;
        if ((this.idevalor == null && other.idevalor != null) || (this.idevalor != null && !this.idevalor.equals(other.idevalor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.siviuq2.entities.Valoracion[ idevalor=" + idevalor + " ]";
    }
    
}
