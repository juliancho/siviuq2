/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.beans;

import com.uniquindio.siviuq.entities.Infofinan;
import com.uniquindio.siviuq.entities.Inforavan;
import com.uniquindio.siviuq.facadeInterface.InfofinanFacadeInterface;
import com.uniquindio.siviuq.facadeInterface.InforavanFacadeInterface;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leon
 */
public class InfofinanFacadeTest {
    private static InfofinanFacadeInterface miFacade= new InfofinanFacade();
    private static InforavanFacadeInterface miFacadeInforavan= new InforavanFacade(); 
        
       
    @BeforeClass
    public static void configurar() throws NamingException{
        miFacade= (InfofinanFacadeInterface)new InitialContext().lookup("java:global/siviuq2/InfofinanFacade!com.uniquindio.siviuq.facadeInterface.InfofinanFacadeInterface");
        miFacadeInforavan= (InforavanFacadeInterface)new InitialContext().lookup("java:global/siviuq2/InforavanFacade!com.uniquindio.siviuq.facadeInterface.InforavanFacadeInterface");
        
    }
    
      
    /**
     * Test of create method, of class UsuarioFacade.
     */
    @Test
    public void testCreate() throws NamingException {
        Inforavan miInforavan=new Inforavan();
        miInforavan.setIdinfava("50");
        miInforavan.setFechapresentacion(null);
        miInforavan.setNumdeinforme(BigInteger.ZERO);
        
        Infofinan miUsuario=new Infofinan();
        miUsuario.setIdinffin(BigDecimal.ZERO);
        miUsuario.setActividad("claro");
        miUsuario.setPresuproyectado(BigInteger.ZERO);
        miUsuario.setIdinfoavan(miInforavan);
        
        miFacadeInforavan.create(miInforavan);
        miFacade.create(miUsuario);
        
        Infofinan miUsuarioRecibido= miFacade.find(miUsuario.getIdinffin());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("creo");
        miFacade.remove(miUsuario);
        miFacadeInforavan.remove(miInforavan);
    }
    
    /**
     * Test of edit method, of class UsuarioFacade.
     */
    @Test
    public void testEdit() throws Exception {
       Inforavan miInforavan=new Inforavan();
        miInforavan.setIdinfava("50");
        miInforavan.setFechapresentacion(null);
        miInforavan.setNumdeinforme(BigInteger.ZERO);
        
        Infofinan miUsuario=new Infofinan();
        miUsuario.setIdinffin(BigDecimal.ZERO);
        miUsuario.setActividad("claro");
        miUsuario.setPresuproyectado(BigInteger.ZERO);
        miUsuario.setIdinfoavan(miInforavan);
        
        miFacadeInforavan.create(miInforavan);
        miFacade.create(miUsuario);
        
        miUsuario.setActividad("no claro");
        
        miFacade.edit(miUsuario);
        
        Infofinan miUsuarioRecibido= miFacade.find(miUsuario.getIdinffin());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("modifico");
        miFacade.remove(miUsuario);
        miFacadeInforavan.remove(miInforavan);  
    }
    
     /**
     * Test of find method, of class UsuarioFacade.
     */
    @Test
    public void testFind() throws Exception {
        Inforavan miInforavan=new Inforavan();
        miInforavan.setIdinfava("50");
        miInforavan.setFechapresentacion(null);
        miInforavan.setNumdeinforme(BigInteger.ZERO);
        
        Infofinan miUsuario=new Infofinan();
        miUsuario.setIdinffin(BigDecimal.ZERO);
        miUsuario.setActividad("claro");
        miUsuario.setPresuproyectado(BigInteger.ZERO);
        miUsuario.setIdinfoavan(miInforavan);
        
        miFacadeInforavan.create(miInforavan);
        miFacade.create(miUsuario);
        
        Infofinan miUsuarioRecibido= miFacade.find(miUsuario.getIdinffin());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("creo");
        miFacade.remove(miUsuario);
        miFacadeInforavan.remove(miInforavan);  
    }
    
    /**
     * Test of remove method, of class UsuarioFacade.
     */
    @Test
    public void testRemove() throws Exception {
        Inforavan miInforavan=new Inforavan();
        miInforavan.setIdinfava("50");
        miInforavan.setFechapresentacion(null);
        miInforavan.setNumdeinforme(BigInteger.ZERO);
        
        Infofinan miUsuario=new Infofinan();
        miUsuario.setIdinffin(BigDecimal.ZERO);
        miUsuario.setActividad("claro");
        miUsuario.setPresuproyectado(BigInteger.ZERO);
        miUsuario.setIdinfoavan(miInforavan);
        
        miFacadeInforavan.create(miInforavan);
        miFacade.create(miUsuario);
        
        Infofinan miUsuarioRecibido= miFacade.find(miUsuario.getIdinffin());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        miFacade.remove(miUsuario);
        miFacadeInforavan.remove(miInforavan);  
        miUsuarioRecibido= miFacade.find(miUsuario.getIdinffin());
        Assert.assertEquals(null,miUsuarioRecibido);
        System.out.println("elimino");
    }
    
     /**
     * Test of findAll method, of class UsuarioFacade.
     */
    @Test
    public void testFindAll() throws Exception {
        List<Infofinan> misUsuariosRecibidos= miFacade.findAll();
        List<Infofinan> misUsuarios=misUsuariosRecibidos;
       
       Inforavan miInforavan=new Inforavan();
        miInforavan.setIdinfava("50");
        miInforavan.setFechapresentacion(null);
        miInforavan.setNumdeinforme(BigInteger.ZERO);
        
        Infofinan miUsuario=new Infofinan();
        miUsuario.setIdinffin(BigDecimal.ZERO);
        miUsuario.setActividad("claro");
        miUsuario.setPresuproyectado(BigInteger.ZERO);
        miUsuario.setIdinfoavan(miInforavan);
        
        
        misUsuarios.add(miUsuario);
        
        miFacadeInforavan.create(miInforavan);
        miFacade.create(miUsuario);
        
        misUsuariosRecibidos= miFacade.findAll();
        
        Assert.assertEquals(misUsuarios,misUsuariosRecibidos);
        
        System.out.println("busco lista");
        miFacade.remove(miUsuario);
        miFacadeInforavan.remove(miInforavan);
        
    }


    /**
     * Test of findRange method, of class UsuarioFacade.
     */
    @Test
    public void testFindRange() throws Exception {
         List<Infofinan> misUsuarios=miFacade.findAll();
         int items=misUsuarios.size();
         
         int[] misItems= new int[2];
         misItems[0]=items;
         misItems[1]=items+1;
         
        Inforavan miInforavan=new Inforavan();
        miInforavan.setIdinfava("50");
        miInforavan.setFechapresentacion(null);
        miInforavan.setNumdeinforme(BigInteger.ZERO);
        
        Infofinan miUsuario=new Infofinan();
        miUsuario.setIdinffin(BigDecimal.ZERO);
        miUsuario.setActividad("claro");
        miUsuario.setPresuproyectado(BigInteger.ZERO);
        miUsuario.setIdinfoavan(miInforavan);
        
        List<Infofinan> miListaUsuario= new ArrayList<Infofinan>();
        miListaUsuario.add(miUsuario);
        
        miFacadeInforavan.create(miInforavan);
        miFacade.create(miUsuario);
        
        
        misUsuarios=miFacade.findRange(misItems);
        
        Assert.assertEquals(misUsuarios, miListaUsuario);
        System.out.println("busco por rango");
       miFacade.remove(miUsuario);
        miFacadeInforavan.remove(miInforavan);
    }
   
    
    /**
     * Test of count method, of class UsuarioFacade.
     */
    @Test
    public void testCount() throws Exception {
       List<Infofinan> misUsuarios = miFacade.findAll();
       int cantidad=misUsuarios.size();
       
       int cantidadRecibida=miFacade.count();
       
       Assert.assertEquals(cantidad, cantidadRecibida);
       
       System.out.println("conto");
    }
}
