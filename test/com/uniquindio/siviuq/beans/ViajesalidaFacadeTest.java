/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.beans;

import com.uniquindio.siviuq.entities.Usuario;
import com.uniquindio.siviuq.entities.Viajesalida;
import com.uniquindio.siviuq.facadeInterface.UsuarioFacadeInterface;
import com.uniquindio.siviuq.facadeInterface.ViajesalidaFacadeInterface;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leon
 */
public class ViajesalidaFacadeTest {
    private static ViajesalidaFacadeInterface miFacade= new ViajesalidaFacade(); 
        
       
    @BeforeClass
    public static void configurar() throws NamingException{
        
        miFacade= (ViajesalidaFacadeInterface)new InitialContext().lookup("java:global/siviuq2/ViajesalidaFacade!com.uniquindio.siviuq.facadeInterface.ViajesalidaFacadeInterface");
        System.out.println("crea una instancia del bean");
    }
    
      
    /**
     * Test of create method, of class UsuarioFacade.
     */
    @Test
    public void testCreate() throws NamingException {
        Viajesalida miUsuario=new Viajesalida();
        miUsuario.setIdviaje("500");
        miUsuario.setLugar("cancha");
        miUsuario.setJustificacion("por que si");
               
        miFacade.create(miUsuario);
        
        Viajesalida miUsuarioRecibido= miFacade.find(miUsuario.getIdviaje());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("creo");
        miFacade.remove(miUsuario);
    }
    
    /**
     * Test of edit method, of class UsuarioFacade.
     */
    @Test
    public void testEdit() throws Exception {
       Viajesalida miUsuario=new Viajesalida();
        miUsuario.setIdviaje("500");
        miUsuario.setLugar("cancha");
        miUsuario.setJustificacion("por que si");
               
        miFacade.create(miUsuario);
        miUsuario.setLugar("auditorio");
        
        miFacade.edit(miUsuario);
        
        Viajesalida miUsuarioRecibido= miFacade.find(miUsuario.getIdviaje());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("modifico");
        miFacade.remove(miUsuario);   
    }
    
     /**
     * Test of find method, of class UsuarioFacade.
     */
    @Test
    public void testFind() throws Exception {
        Viajesalida miUsuario=new Viajesalida();
        miUsuario.setIdviaje("500");
        miUsuario.setLugar("cancha");
        miUsuario.setJustificacion("por que si");
               
        miFacade.create(miUsuario);
        
        Viajesalida miUsuarioRecibido= miFacade.find(miUsuario.getIdviaje());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("busco");
        miFacade.remove(miUsuario);  
    }
    
    /**
     * Test of remove method, of class UsuarioFacade.
     */
    @Test
    public void testRemove() throws Exception {
        Viajesalida miUsuario=new Viajesalida();
        miUsuario.setIdviaje("500");
        miUsuario.setLugar("cancha");
        miUsuario.setJustificacion("por que si");
               
        miFacade.create(miUsuario);
        
        Viajesalida miUsuarioRecibido= miFacade.find(miUsuario.getIdviaje());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        miFacade.remove(miUsuario);
        miUsuarioRecibido= miFacade.find(miUsuario.getIdviaje());
        Assert.assertEquals(null,miUsuarioRecibido);
        System.out.println("elimino");
    }
    
     /**
     * Test of findAll method, of class UsuarioFacade.
     */
    @Test
    public void testFindAll() throws Exception {
        List<Viajesalida> misUsuariosRecibidos= miFacade.findAll();
        List<Viajesalida> misUsuarios=misUsuariosRecibidos;
       
        Viajesalida miUsuario=new Viajesalida();
        miUsuario.setIdviaje("500");
        miUsuario.setLugar("cancha");
        miUsuario.setJustificacion("por que si");
             
        misUsuarios.add(miUsuario);
        miFacade.create(miUsuario);
        
        misUsuariosRecibidos= miFacade.findAll();
        
        Assert.assertEquals(misUsuarios,misUsuariosRecibidos);
        
        System.out.println("busco lista");
        miFacade.remove(miUsuario);
        
    }


    /**
     * Test of findRange method, of class UsuarioFacade.
     */
    @Test
    public void testFindRange() throws Exception {
         List<Viajesalida> misUsuarios=miFacade.findAll();
         int items=misUsuarios.size();
         
         int[] misItems= new int[2];
         misItems[0]=items;
         misItems[1]=items+1;
         
        Viajesalida miUsuario=new Viajesalida();
        miUsuario.setIdviaje("500");
        miUsuario.setLugar("cancha");
        miUsuario.setJustificacion("por que si");
           
        List<Viajesalida> miListaUsuario= new ArrayList<Viajesalida>();
        miListaUsuario.add(miUsuario);
        miFacade.create(miUsuario);
        
        misUsuarios=miFacade.findRange(misItems);
        
        Assert.assertEquals(misUsuarios, miListaUsuario);
        System.out.println("busco por rango");
        miFacade.remove(miUsuario);
    }
   
    
    /**
     * Test of count method, of class UsuarioFacade.
     */
    @Test
    public void testCount() throws Exception {
       List<Viajesalida> misUsuarios = miFacade.findAll();
       int cantidad=misUsuarios.size();
       
       int cantidadRecibida=miFacade.count();
       
       Assert.assertEquals(cantidad, cantidadRecibida);
       
       System.out.println("conto");
    }
}
