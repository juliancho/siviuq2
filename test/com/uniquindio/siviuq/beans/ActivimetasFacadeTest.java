/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.beans;

import com.uniquindio.siviuq.entities.Activimetas;
import com.uniquindio.siviuq.entities.Inforavan;
import com.uniquindio.siviuq.facadeInterface.ActivimetasFacadeInterface;
import com.uniquindio.siviuq.facadeInterface.InforavanFacadeInterface;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leon
 */
public class ActivimetasFacadeTest {
    private static ActivimetasFacadeInterface miFacade= new ActivimetasFacade();
    private static InforavanFacadeInterface miFacadeInforavan= new InforavanFacade();
   
    @BeforeClass
    public static void configurar() throws Exception{
        miFacade= (ActivimetasFacadeInterface)new InitialContext().lookup("java:global/siviuq2/ActivimetasFacade!com.uniquindio.siviuq.facadeInterface.ActivimetasFacadeInterface");
        System.out.println("crea una instancia del bean Programa");
        miFacadeInforavan= (InforavanFacadeInterface)new InitialContext().lookup("java:global/siviuq2/InforavanFacade!com.uniquindio.siviuq.facadeInterface.InforavanFacadeInterface");
        System.out.println("crea una instancia del bean facultad");
    
    }
    
    /**
     * Test of create method, of class FacultadFacade.
     */
    @Test
    public void testCreate() throws NamingException {
        
        Inforavan miUsuario=new Inforavan();
        miUsuario.setIdinfava("50");
        miUsuario.setFechapresentacion(null);
        miUsuario.setNumdeinforme(BigInteger.ZERO);
        
       Activimetas miActivimetas=new Activimetas();
       miActivimetas.setIdactmetas("50");
       miActivimetas.setMetaprogramada("julio");
       miActivimetas.setIdinfoavan(miUsuario);
        
        miFacadeInforavan.create(miUsuario);
        miFacade.create(miActivimetas);

        Activimetas miActivimetasRecibido= miFacade.find(miActivimetas.getIdactmetas());
        
        Assert.assertEquals(miActivimetas,miActivimetasRecibido);
        System.out.println("paso la prueba creo");
        
        miFacade.remove(miActivimetas);
        miFacadeInforavan.remove(miUsuario);
        
    }
    
    /**
     * Test of edit method, of class UsuarioFacade.
     */
    @Test
    public void testEdit() throws Exception {
       Inforavan miUsuario=new Inforavan();
        miUsuario.setIdinfava("50");
        miUsuario.setFechapresentacion(null);
        miUsuario.setNumdeinforme(BigInteger.ZERO);
        
       Activimetas miActivimetas=new Activimetas();
       miActivimetas.setIdactmetas("50");
       miActivimetas.setMetaprogramada("julio");
       miActivimetas.setIdinfoavan(miUsuario);
        
        miFacadeInforavan.create(miUsuario);
        miFacade.create(miActivimetas);
        
        miActivimetas.setMetaprogramada("agosto");
        miFacade.edit(miActivimetas);

        Activimetas miActivimetasRecibido= miFacade.find(miActivimetas.getIdactmetas());
        
        Assert.assertEquals(miActivimetas,miActivimetasRecibido);
        System.out.println("paso la prueba modifico");
        
        miFacade.remove(miActivimetas);
        miFacadeInforavan.remove(miUsuario);
          
    }
    
    /**
     * Test of find method, of class UsuarioFacade.
     */
    @Test
    public void testFind() throws Exception {
        Inforavan miUsuario=new Inforavan();
        miUsuario.setIdinfava("50");
        miUsuario.setFechapresentacion(null);
        miUsuario.setNumdeinforme(BigInteger.ZERO);
        
       Activimetas miActivimetas=new Activimetas();
       miActivimetas.setIdactmetas("50");
       miActivimetas.setMetaprogramada("julio");
       miActivimetas.setIdinfoavan(miUsuario);
        
        miFacadeInforavan.create(miUsuario);
        miFacade.create(miActivimetas);

        Activimetas miActivimetasRecibido= miFacade.find(miActivimetas.getIdactmetas());
        
        Assert.assertEquals(miActivimetas,miActivimetasRecibido);
        System.out.println("paso la prueba creo");
        
        miFacade.remove(miActivimetas);
        miFacadeInforavan.remove(miUsuario);  
    }
    
    /**
     * Test of remove method, of class UsuarioFacade.
     */
    @Test
    public void testRemove() throws Exception {
        Inforavan miUsuario=new Inforavan();
        miUsuario.setIdinfava("50");
        miUsuario.setFechapresentacion(null);
        miUsuario.setNumdeinforme(BigInteger.ZERO);
        
       Activimetas miActivimetas=new Activimetas();
       miActivimetas.setIdactmetas("50");
       miActivimetas.setMetaprogramada("julio");
       miActivimetas.setIdinfoavan(miUsuario);
        
        miFacadeInforavan.create(miUsuario);
        miFacade.create(miActivimetas);

        Activimetas miActivimetasRecibido= miFacade.find(miActivimetas.getIdactmetas());
        
        Assert.assertEquals(miActivimetas,miActivimetasRecibido);
        miFacade.remove(miActivimetas);
        miFacadeInforavan.remove(miUsuario);  
        miActivimetasRecibido= miFacade.find(miActivimetas.getIdactmetas());
        Assert.assertEquals(null,miActivimetasRecibido);
        System.out.println("elimino");
    }
    
    /**
     * Test of findAll method, of class UsuarioFacade.
     */
    @Test
    public void testFindAll() throws Exception {
        List<Activimetas> misActivimetasRecibidos= miFacade.findAll();
        List<Activimetas> misActivimetas=misActivimetasRecibidos;
       
        Inforavan miUsuario=new Inforavan();
        miUsuario.setIdinfava("50");
        miUsuario.setFechapresentacion(null);
        miUsuario.setNumdeinforme(BigInteger.ZERO);
        
       Activimetas miActivimetas=new Activimetas();
       miActivimetas.setIdactmetas("50");
       miActivimetas.setMetaprogramada("julio");
       miActivimetas.setIdinfoavan(miUsuario);
        
       misActivimetas.add(miActivimetas);
       
        miFacadeInforavan.create(miUsuario);
        miFacade.create(miActivimetas);
        
        misActivimetasRecibidos= miFacade.findAll();
        
        Assert.assertEquals(misActivimetas,misActivimetasRecibidos);
        
        System.out.println("busco lista");
        
        miFacade.remove(miActivimetas);
        miFacadeInforavan.remove(miUsuario);  
    }
    
    /**
     * Test of findRange method, of class UsuarioFacade.
     */
    @Test
    public void testFindRange() throws Exception {
         List<Activimetas> misActivimetas=miFacade.findAll();
         int items=misActivimetas.size();
         
         int[] misItems= new int[2];
         misItems[0]=items;
         misItems[1]=items+1;
         
        Inforavan miUsuario=new Inforavan();
        miUsuario.setIdinfava("50");
        miUsuario.setFechapresentacion(null);
        miUsuario.setNumdeinforme(BigInteger.ZERO);
        
       Activimetas miActivimetas=new Activimetas();
       miActivimetas.setIdactmetas("50");
       miActivimetas.setMetaprogramada("julio");
       miActivimetas.setIdinfoavan(miUsuario);
        
       List<Activimetas> miListaActivimetas= new ArrayList<Activimetas>();
        miListaActivimetas.add(miActivimetas);
       
        miFacadeInforavan.create(miUsuario);
        miFacade.create(miActivimetas);
        
        
        misActivimetas=miFacade.findRange(misItems);
        
        Assert.assertEquals(misActivimetas, miListaActivimetas);
        System.out.println("busco por rango");
        miFacade.remove(miActivimetas);
        miFacadeInforavan.remove(miUsuario);
    }
    
    /**
     * Test of count method, of class UsuarioFacade.
     */
    @Test
    public void testCount() throws Exception {
       List<Activimetas> misUsuarios = miFacade.findAll();
       int cantidad=misUsuarios.size();
       
       int cantidadRecibida=miFacade.count();
       
       Assert.assertEquals(cantidad, cantidadRecibida);
       
       System.out.println("conto");
    }
}
