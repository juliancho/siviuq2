/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.siviuq.beans;

import com.uniquindio.siviuq.entities.Anexo;
import com.uniquindio.siviuq.entities.Usuario;
import com.uniquindio.siviuq.facadeInterface.AnexoFacadeInterface;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leon
 */
public class AnexoFacadeTest {
    private static AnexoFacadeInterface miFacade= new AnexoFacade(); 
        
       
    @BeforeClass
    public static void configurar() throws NamingException{
        
        miFacade= (AnexoFacadeInterface)new InitialContext().lookup("java:global/siviuq2/AnexoFacade!com.uniquindio.siviuq.facadeInterface.AnexoFacadeInterface");
        System.out.println("crea una instancia del bean");
    }
    
      
    /**
     * Test of create method, of class UsuarioFacade.
     */
    @Test
    public void testCreate() throws NamingException {
        Anexo miUsuario=new Anexo();
        miUsuario.setIdanexo("50");
        miUsuario.setAnexoruta("ruta");
       
       
        miFacade.create(miUsuario);
        
        Anexo miUsuarioRecibido= miFacade.find(miUsuario.getIdanexo());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("creo");
        miFacade.remove(miUsuario);
    }
    
    /**
     * Test of edit method, of class UsuarioFacade.
     */
    @Test
    public void testEdit() throws Exception {
       Anexo miUsuario=new Anexo();
        miUsuario.setIdanexo("50");
        miUsuario.setAnexoruta("ruta");
       
       
        miFacade.create(miUsuario);
        
        miUsuario.setAnexoruta("miruta 2");
      
        miFacade.edit(miUsuario);
        
        Anexo miUsuarioRecibido= miFacade.find(miUsuario.getIdanexo());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("modifico");
        miFacade.remove(miUsuario);   
    }
    
     /**
     * Test of find method, of class UsuarioFacade.
     */
    @Test
    public void testFind() throws Exception {
        Anexo miUsuario=new Anexo();
        miUsuario.setIdanexo("50");
        miUsuario.setAnexoruta("ruta");
       
       
        miFacade.create(miUsuario);
        
        Anexo miUsuarioRecibido= miFacade.find(miUsuario.getIdanexo());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        System.out.println("busco");
        miFacade.remove(miUsuario);  
    }
    
    /**
     * Test of remove method, of class UsuarioFacade.
     */
    @Test
    public void testRemove() throws Exception {
        Anexo miUsuario=new Anexo();
        miUsuario.setIdanexo("50");
        miUsuario.setAnexoruta("ruta");
       
       
        miFacade.create(miUsuario);
        
        Anexo miUsuarioRecibido= miFacade.find(miUsuario.getIdanexo());
        
        Assert.assertEquals(miUsuario,miUsuarioRecibido);
        miFacade.remove(miUsuario);
        miUsuarioRecibido= miFacade.find(miUsuario.getIdanexo());
        Assert.assertEquals(null,miUsuarioRecibido);
        System.out.println("elimino");
    }
    
     /**
     * Test of findAll method, of class UsuarioFacade.
     */
    @Test
    public void testFindAll() throws Exception {
        List<Anexo> misUsuariosRecibidos= miFacade.findAll();
        List<Anexo> misUsuarios=misUsuariosRecibidos;
       
        
        Anexo miUsuario=new Anexo();
        miUsuario.setIdanexo("50");
        miUsuario.setAnexoruta("ruta");
      
        misUsuarios.add(miUsuario);
        miFacade.create(miUsuario);
        
        misUsuariosRecibidos= miFacade.findAll();
        
        Assert.assertEquals(misUsuarios,misUsuariosRecibidos);
        
        System.out.println("busco lista");
        miFacade.remove(miUsuario);
        
    }


    /**
     * Test of findRange method, of class UsuarioFacade.
     */
    @Test
    public void testFindRange() throws Exception {
         List<Anexo> misUsuarios=miFacade.findAll();
         int items=misUsuarios.size();
         
         int[] misItems= new int[2];
         misItems[0]=items;
         misItems[1]=items+1;
         
       Anexo miUsuario=new Anexo();
        miUsuario.setIdanexo("50");
        miUsuario.setAnexoruta("ruta");
       
        List<Anexo> miListaUsuario= new ArrayList<Anexo>();
        miListaUsuario.add(miUsuario);
        miFacade.create(miUsuario);
        
        misUsuarios=miFacade.findRange(misItems);
        
        Assert.assertEquals(misUsuarios, miListaUsuario);
        System.out.println("busco por rango");
        miFacade.remove(miUsuario);
    }
   
    
    /**
     * Test of count method, of class UsuarioFacade.
     */
    @Test
    public void testCount() throws Exception {
       List<Anexo> misUsuarios = miFacade.findAll();
       int cantidad=misUsuarios.size();
       
       int cantidadRecibida=miFacade.count();
       
       Assert.assertEquals(cantidad, cantidadRecibida);
       
       System.out.println("conto");
    }
}
